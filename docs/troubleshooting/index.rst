故障排除
================

.. toctree::
   :maxdepth: 1


   failed-list-devices.rst 
   failed-open-device.rst 
   failed-imaging.rst 
   failed-external-trigger.rst 
   fail-trigger-error.rst 
   fps-differ-from-input-frequence.rst 
   fetchframe-lost-frame.rst
   fps-error.rst 
   bad-depth-images.rst





