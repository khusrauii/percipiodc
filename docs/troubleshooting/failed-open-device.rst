

无法打开相机怎么办？
=====================


问题（USB 相机）
+++++++++++++++++++++

使用 Windows PowerShell 运行可执行文件 ListDevices.exe，可枚举到相机，但是出现报错，并且 vender 和 model 信息为空，如下图所示。

.. figure:: ../image/list-information-error.png
    :align: center
    :alt: 枚举信息异常
    :figclass: align-center

    枚举信息异常


原因及解决方法（USB 相机）
+++++++++++++++++++++++++++++


可能的原因及其处理措施如下：

- 相机被占用。
  
  请排查是否有其他进程占用了该相机（如 Percipio Viewer 工具）。若是其他进程占用了该相机，关闭进程即可。
  
- USB 连接线太长。
  
  不建议使用过长的 USB 连接线（超过 3 米），请更换短线后再尝试打开相机。


问题（网络相机）
+++++++++++++++++


网络相机上电并完成初始化后，可枚举到相机，但打开相机时，出现报错，如下图所示。

.. figure:: ../image/open-device-error-ethernet.png
    :align: center
    :alt: 打开相机时报错
    :figclass: align-center

    打开相机时报错


原因及解决方法（网络相机）
+++++++++++++++++++++++++++++++


可能的原因及其处理措施如下：

- 相机被占用。
  
  请排查是否有本机上的其他进程（如 Percipio Viewer 工具）或其他上位机占用了该相机。若相机被占用，解除占用即可。
  
- 相机内写入了第三方程序。
  
  请联系 Percipio 售后支持。



