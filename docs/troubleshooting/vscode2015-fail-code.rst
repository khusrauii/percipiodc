关于 Visual Studio 2015 安装的常见问题
================================================

推荐使用 Visual Studio 2015 作为 SDK 的编译工具，如需安装包，请联系 Percipio 售后支持。

安装 Visual Studio 2015 时，系统默认不安装 Visual C++ 运行库，需要自己手动添加，具体操作步骤如下：

1. 在菜单栏依次点击 “文件” > “新建” > “项目”。
   
   弹出 “新建项目” 对话框。

   .. figure:: ../image/program.png
       :align: center
       :alt: 新建项目对话框
       :figclass: align-center
       
       新建项目对话框

2. 在 “新建项目” 对话框里，依次点击 “模块” > “Visual C++” > “Install Visual C++ 2015 Tools for Windows Desktop”。

3. 根据安装指导完成安装，安装完成后如下图所示。

   .. figure:: ../image/succeed-install-visualc++.png
       :align: center
       :alt: Visual C++ 运行库安装成功
       :figclass: align-center
       
       Visual C++ 运行库安装成功



