.. _failed-external-trigger-label:


硬触发模式下，相机不采图怎么办？
===================================


问题
++++++

外接触发信号后，设置相机工作在硬触发模式下，相机不采集图像。

.. figure:: ../image/lost-frames.png
    :align: center
    :alt: 未采集到图像
    :figclass: align-center
   
       

原因及解决方法
++++++++++++++++++

可能的原因及其处理措施如下：

- **外部触发信号不符合要求**

  Percipio 深度相机触发信号默认是“ **下降沿触发** ”，接收输入为脉冲方波，方波应保持低电平 **10～30 毫秒**，同时为了避免错误触发，信号下降时间应“ **不超过 5 微秒** ”。

  请选择符合以上要求的外部触发源信号，如有需要可购买 Percipio 生产的外部触发源。


- **Trigger 接线不正确**

  未给 Trigger 供电或输入信号线接到错误的 Pin 脚。
  
  根据相机触发接口和引脚定义，正确接线。

  【USB 相机】

    .. figure:: ../image/M8-5pin-connector.svg
        :width: 240px
        :align: center
        :alt: USB相机触发接口
        :figclass: align-center
   
        USB 相机触发接口

    .. list-table::
       :header-rows: 1
    
       * - 序号
         - 名称
         - 功能描述
         - 配套线芯颜色
       * - 1
         - Trig_GND
         - 电源地
         - 黑色
       * - 2
         - Trig_GND
         - 电源地
         - 棕色
       * - 3
         - Trig_OUT
         - 触发输出信号
         - 红色
       * - 4
         - Trig_IN
         - 触发输入信号
         - 橘色
       * - 5
         - Trig_POWER（12V～24V）
         - 触发电路电源正
         - 黄色

  【网络相机】


    .. figure:: ../image/M8-6pin-connector.svg
        :width: 240px
        :align: center
        :alt: M8 网络相机触发接口
        :figclass: align-center
    
        M8 网络相机触发接口
   
    .. list-table::
       :header-rows: 1
    
       * - 序号
         - 名称
         - 功能描述
         - 配套线芯颜色
       * - 1
         - Trig_OUT
         - 触发输出信号
         - 黑色
       * - 2
         - P_24V
         - 电源正
         - 棕色
       * - 3
         - P_GND
         - 电源地
         - 红色
       * - 4
         - Trig_POWER（12V～24V） 
         - 触发电路电源正
         - 橘色
       * - 5
         - Trig_GND
         - 触发电路电源地
         - 黄色
       * - 6
         - Trig_IN
         - 触发输入信号
         - 绿色




    .. figure:: ../image/M8-6pin-connector-h.svg
        :width: 240px
        :align: center
        :alt: 6 芯推拉自锁航插头
        :figclass: align-center
    
        6 芯推拉自锁航插头
   
    .. list-table::
       :header-rows: 1
    
       * - 序号
         - 名称
         - 功能描述
         - 配套线芯颜色
       * - 1
         - P_24V
         - 电源正
         - 红色
       * - 2
         - Trig_IN
         - 触发输入信号
         - 黄色
       * - 3
         - Trig_OUT
         - 触发输出信号
         - 蓝色
       * - 4
         - Trig_POWER（12V～24V）
         - 触发电路电源正
         - 绿色
       * - 5
         - Trig_GND
         - 触发电路电源地
         - 白色
       * - 6
         - P_GND
         - 电源地
         - 黑色

    .. figure:: ../image/M12-A-Code-8pin-connector-power.svg
        :width: 240px
        :align: center
        :alt: M12 A-Code 网络相机触发接口
        :figclass: align-center
    
        M12 A-Code 网络相机触发接口
   
    .. list-table::
       :header-rows: 1
    
       * - 序号
         - 名称
         - 功能描述
         - 配套线芯颜色
       * - 1
         - Trig_OUT
         - 触发输出信号
         - 白色
       * - 2
         - P_24V
         - 电源正
         - 棕色
       * - 3
         - P_GND
         - 电源地
         - 绿色
       * - 4
         - Trig_POWER（12V～24V）
         - 触发电路电源正
         - 黄色
       * - 5
         - Trig_GND
         - 触发电路电源地
         - 灰色
       * - 6
         - NC
         - 保留
         - 粉色
       * - 7
         - Trig_IN
         - 触发输入信号
         - 蓝色
       * - 8
         - NC
         - 保留
         - 红色

