

软触发模式下，相机报错 error:-1014 怎么办？
=============================================

问题
+++++

在软触发模式下使能相机，相机长时间未采集到图像，并报错 error:-1014。

.. figure:: ../image/lost-frames.png 
    :align: center
    :alt: 未采集到图像
    :figclass: align-center
   
       

原因及解决方法
++++++++++++++++++

可能的原因及其处理措施如下：

- **超时时间设置过短**

  通过以下代码，适当增加超时时间。其中 timeout 为超时时间，单位为毫秒。

  ::

      int err = TYFetchFrame(hDevice, &frame,timeout);

- **数据传输时丢失**

 【USB 相机】
 
 - 减少与上位机连接的 USB 设备。
 - 减少数据量（只输出较低分辨率的深度图）。
 - 选择较高性能的上位机。


 【网络相机】
  
 - 通过以下代码，开启网络重传。
 
   ::
    
      ASSERT_OK(TYSetBool(hDevice,TY_COMPONENT_DEVICE,TY_BOOL_GVSP_RESEND,true))
 
 - 建议更换性能好的上位机或者减少数据量。
 
