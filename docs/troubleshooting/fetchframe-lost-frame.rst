自由采集模式下，相机跳帧怎么办？
=====================================

问题
+++++++

使用 Windows Power Shell 运行可执行文件 SimpleView_FetchFrame.exe，相机有跳帧严重的现象，图像不流畅。




原因及解决方法
+++++++++++++++++++++


可能的原因及其处理措施如下：

- **上位机系统处于低性能模式**

  上位机系统处于低性能模式时，数据接收不稳定。

  根据操作系统，将上位机系统设置为高性能模式。

 **【Windows 系统】**

  请按照以下步骤，进行上位机系统设置：
     
  1. 将上位机系统设置中的 “接收缓冲区” 的值设置为最大。
          
          .. figure:: ../image/set-receive-buffer.gif
              :align: center
              :alt: 设置接收缓冲区
              :figclass: align-center

              设置接收缓冲区

  2. 将上位机系统设置中 “电源选项” 设置为 “平衡” 模式。
          
          .. figure:: ../image/power-plan.png
              :align: center
              :alt: 设置平衡模式
              :figclass: align-center

              设置平衡模式

 **【Linux 系统】**
      
  Linux 系统默认 CPU 性能为 powersave 模式，需要将其设置为 performance 模式。
  
  请按照以下步骤，设置 performance 模式：
        
  1. 在终端输入以下命令行，查看上位机 CPU 性能。
          
     ::

         cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor

  2. 若查询到上位机 CPU 性能为 powersave，则在终端输入以下命令，安装软件 indicator-cpufreq。
           
     ::

         sudo apt-get install indicator-cpufreq

  3. 重启上位机。
   
  4. 点击界面右上角图标，将 CPU 性能切换到 performance。

     .. figure:: ../image/set-cpu-performance.png
         :align: center
         :alt: 切换 CPU 性能
         :figclass: align-center
       
         切换 CPU 性能
    




