.. _TS460-E2-label:


TS460-E2
============

.. figure:: ../image/TM460-01.png
    :width: 400px
    :align: center
    :alt: TS460 外观
    :figclass: align-center

    TS460-E2 外观


测量指标
------------

.. list-table::
   :header-rows: 1

   * - 项目
     - 单位
     - 范围
     - 备注
   * - 测量距离
     - mm
     - 100 ~ 2400
     - 与补光亮度和环境光相关
   * - 深度近视场
     - mm
     - 125 x 95 @ 100
     - H/V: 65°/50°
   * - 深度远视场
     - mm
     - 2730 x 2035 @ 2400
     - H/V: 65°/50° 
   * - 精度误差 Z
     - mm
     - 5 ～ 20
     - 与距离呈非线性关系

.. include:: TM460-E2.rst
   :start-after: TM460-E2-reuse

