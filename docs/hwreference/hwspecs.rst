
.. _camera-param-label:

产品规格
=============

Percipio 深度相机提供多种外壳结构、硬件接口和测量精度等参数配置，以满足不同场景的需求。相机分采用 :ref:`主动双目 <active-stereo-label>` 和 :ref:`飞行时间 <tof-label>` （ToF）技术原理的深度相机，同时提供 USB 接口和网络接口两种数据输出形态作为选择。

该部分主要介绍主动双目 USB 深度相机、主动双目网络深度相机和 ToF 网络深度相机的部分规格。点击型号链接即可阅读各款深度相机的图像传感器配置、接口配置、整机功耗、机械尺寸等详细数据。

主动双目 USB 深度相机
---------------------------             

.. list-table:: 
   :header-rows: 1

   * - 系列
     - 型号
     - Depth图像
     - Color图像
     - 曝光模式
     - 基线
     - 硬件触发
   * - DS
     - :ref:`DS462-label`
     - 29fps @ 640*480
     - NONE
     - RS
     - 25
     - NO
   * - FM  
     - :ref:`FM830-RI-label`
     - 13fps @ 1280*960
     - 1280*960
     - RS
     - 55
     - YES   
   * - FM  
     - :ref:`FM831-RI-label`
     - 13fps @ 1280*960
     - 1280*960
     - RS
     - 79
     - YES
   * - FM  
     -  :ref:`FM860-GI-label`
     - 29fps @ 1280*960
     - 1280*960
     - GS
     - 79
     - YES
   * - FM  
     - :ref:`FM810-TIX-label` 
     - 5fps @ 1280*960
     - 1280*960
     - RS
     - 55
     - YES
   * - FM  
     -  :ref:`FM811-TIX-label`
     - 5fps @1280*960
     - 1280*960
     - RS
     - 79
     - YES


.. toctree::
   :hidden:
   :maxdepth: 1

   DS462-U2
   FM830-RI-U2
   FM831-RI-U2
   FM860-GI-U3
   FM810-TIX-U2
   FM811-TIX-U2




主动双目网络深度相机
---------------------------


.. list-table:: 
   :header-rows: 1

   * - 系列
     - 型号
     - Depth图像
     - Color图像
     - 曝光模式
     - 基线
     - 硬件触发
   * - FS  
     - :ref:`FS820-E1-label`
     - 7fps @1280*800
     - 1920*1080
     - GS
     - 50
     - YES
   * - FM  
     - :ref:`FM850-E1-label`
     - 13fps @1280*960
     - 1280*960
     - GS
     - 55
     - YES
   * - FM  
     - :ref:`FM850-E2-label`
     - 13fps @1280*960
     - 1280*960
     - GS
     - 55
     - YES
   * - FM  
     - :ref:`FM850-WH-E2-label`
     - 13fps @1280*960
     - 1280*960
     - GS
     - 55
     - YES
   * - FM  
     - :ref:`FM851-E1-label`
     - 16fps @1280*960
     - 1280*960
     - GS
     - 79
     - YES
   * - FM  
     - :ref:`FM853-E1-label`
     - 16fps @1280*960
     - 2592*1944
     - GS
     - 150
     - YES
   * - FM-IX  
     - :ref:`FM811-IX-E1-label`
     - 5fps @1280*960
     - 1280*960
     - GS
     - 79
     - YES
   * - FM-IX  
     - :ref:`FM813-IX-E1-label`
     - 5fps @1280*960
     - 2592*1944
     - GS
     - 150
     - YES
   * - FM-IX  
     - :ref:`FM813-L2-E1-label`
     - 3fps @1280*960
     - 2592*1944
     - GS
     - 150
     - YES
   * - PS 
     - :ref:`PS801-N-E1-label`
     - 1fps @1280*960
     - 2560*1920
     - RS
     - 100
     - YES
   * - PS 
     - :ref:`PS801-E1-label`
     - 1fps @1280*960
     - 2560*1920
     - RS
     - 100
     - YES
   * - PM  
     - :ref:`PM801-E1-label`
     - 1fps @1280*960
     - 2592*1944
     - GS
     - 300
     - YES
   * - PM  
     - :ref:`PM802-E1-label`
     - 1fps @1280*960
     - 2592*1944
     - GS
     - 450
     - YES


.. toctree::
   :hidden:
   :maxdepth: 1

   FS820-E1
   FM850-E1
   FM850-E2
   FM850-WH-E2
   FM851-E1
   FM851-E2
   FM853-E1
   FM811-IX-E1
   FM813-IX-E1
   FM813-L2-E1
   PS801-N-E1
   PS801-E1
   PM801-E1
   PM802-E1





ToF 网络深度相机
---------------------------


.. list-table:: 
   :header-rows: 1

   * - 系列
     - 型号
     - Depth图像
     - Color图像
     - 曝光模式
     - 基线
     - 硬件触发
   * - TM 
     - :ref:`TM460-E2-label`
     - 30fps @640*480
     - 1920*1080
     - ——
     - ——
     - NO 
   * - TM  
     - :ref:`TM461-E2-label`
     - 30fps @640*480
     - 1920*1080
     - ——
     - ——
     - NO  
   * - TL  
     - :ref:`TL460-S1-E1-label`
     - 30fps @640*480
     - 1920*1080
     - ——
     - ——
     - YES   


本文档仅提供主要产品的信息介绍，部分相机型号不在该列表中，如需了解更多产品信息或者需要依据具体场景定制相机，请邮件咨询 support@percipio.xyz。



.. toctree::
   :hidden:
   :maxdepth: 1

   TM460-E2
   TM461-E2
   TL460-S1-E1
