.. 
  注意：此文件中的 label 不可以删除，位置不可以变动，不要在已有的label之间插入新的label。


.. important ::

  #. 彩色图可以与深度图实现点对点对齐，详情请参考示例程序 SimpleView_Registration 或者查看 API 指南。
  #. 彩色图与深度图实现百分百 **同时曝光，严格同步**。


接口说明
--------

**网络接口**

该相机数据传输采用千兆以太网，网络接口采用 M12 A-Code 8 孔航空接口，如下图所示。


.. figure:: ../image1/M12-A-Code-8pin-connector-net1.svg
    :width: 200px
    :align: center
    :alt: 网络接口
    :figclass: align-center

    网络接口



**电源及触发接口**    

该相机的电源及触发接口采用 M12 A-Code 8 针航空接口，如下图所示。

.. figure:: ../image1/M12-A-Code-8pin-connector-power.svg
    :width: 200px
    :align: center
    :alt: 电源及触发接口 
    :figclass: align-center

    电源及触发接口

.. list-table:: 电源及触发接口引脚说明
   :header-rows: 1

   * - 序号
     - 名称
     - 功能描述
     - 配套线芯颜色
   * - 1
     - Trig_OUT
     - 触发输出信号
     - 白色
   * - 2
     - P_24V
     - 电源正
     - 棕色
   * - 3
     - P_GND
     - 电源地
     - 绿色
   * - 4
     - Trig_POWER 
     - 触发电路电源正
     - 黄色
   * - 5
     - Trig_GND
     - 触发电路电源地
     - 灰色
   * - 6
     - NC
     - 保留
     - 粉色
   * - 7
     - Trig_IN
     - 触发输入信号
     - 蓝色
   * - 8
     - NC
     - 保留
     - 红色


.. note::

    该接口的引脚号与接口信号一一对应，配套线芯的颜色请以实物为准。

.. list-table:: 触发信号电气指标
   :width: 695px  
   :header-rows: 1

   * - 项目
     - 最小值
     - 典型值
     - 最大值
   * - Trig_POWER 电压 (V)
     - 11.4
     - --
     - 25.2
   * - Trig_OUT 高电压 (V)
     - 11.4
     - --
     - 25.2
   * - Trig_OUT 低电压 (V)
     - -0.3
     - 0
     - 0.4
   * - Trig_IN 高电压 (V)
     - 11.4
     - --
     - 25.2
   * - Trig_IN 低电压 (V)
     - -0.3
     - 0
     - 0.4



**触发电路原理**

.. figure:: ../image/triggersch.png
    :width: 550px
    :align: center
    :alt: 触发电路参考图
    :figclass: align-center

    触发电路参考图

.. important ::

  #. 触发信号（OUT）最大支持同时驱动两台同型号相机，如需驱动更多设备，建议增加信号中继设备。
  #. 触发信号（IN/OUT）默认为下降沿触发，接收输入为脉冲方波，方波应保持低电平 **10~30 毫秒**。
  #. 为避免错误触发，下降沿信号下降时间 **不超过 5 微秒** 。触发频率不能超过设备处理能力（即连续模式的帧率），否则相机会丢弃触发信号，不做处理。


**指示灯**

.. list-table:: 指示灯说明
   :header-rows: 1

   * - 颜色
     - 名称
     - 功能描述
   * - 红色
     - 相机状态指示灯
     - 1Hz 缓慢闪烁表示工作正常
   * - 绿色
     - 网络连接指示灯
     - 常亮表示网络连接在千兆网模式，不亮表示工作在百兆网模式
   * - 黄色
     - 网络传输指示灯
     - 有数据传输时闪烁

电源参数
----------

该相机支持以下两种供电方式：

- PoE 供电
   
  使用 Power Over Ehernet(PoE) 供电，将网线插入 RJ45 插座即可。请使用符合 IEEE802.3at/af 标准的 PoE 为相机供电。

- 外部直流供电
 
  将外部直流电源通过工业航插线缆连接到电源接口，即可为相机供电。供电电压为 24 V，建议使用 24 V 直流电源供电。
  
.. Note::

   外部直流电源和 PoE 供电同时存在时，相机优先选用外部直流电源供电。若此时拔出外部直流电源，相机会切换到 PoE 供电，有可能会重启相机。
