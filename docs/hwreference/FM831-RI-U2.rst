.. _FM831-RI-label:

FM831-RI
==============


.. figure:: ../image/FM831-RI-U2-a.png
    :width: 480px
    :align: center
    :alt: FM831-47RI-U2前视图
    :figclass: align-center

    FM831-RI 前视图

.. figure:: ../image/FM830-RI-U2-b.png
    :width: 480px
    :align: center
    :alt: FM831-47RI-U2后视图
    :figclass: align-center

    FM831-RI 后视图


测量指标
------------

.. list-table::
   :header-rows: 1

   * - 项目
     - 单位
     - 范围
     - 备注
   * - 测量距离
     - mm
     - 700 ~ 3400
     - 与补光和环境光相关
   * - 深度近视场
     - mm
     - 725 x 600 @ 700
     - H/V：约 54°/46°
   * - 深度远视场
     - mm
     - 3700 x 2845 @ 3400
     - H/V：约 57°/46°
   * - 精度误差 Z 
     - mm
     - 6 @ 1500
     - 与距离呈非线性关系
   * - 精度误差 X/Y
     - mm
     - 16.17 @ 1500
     - 与距离呈非线性关系

.. include:: FM830-RI-U2.rst 
   :start-after: FM830-RI-U2-reuse


