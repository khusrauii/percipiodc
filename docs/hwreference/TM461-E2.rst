.. _TM461-E2-label:


TM461-E2
============

.. figure:: ../image/TM461-01.png
    :width: 400px
    :align: center
    :alt: TM460 外观
    :figclass: align-center

    TM461-E2 外观


.. figure:: ../image/TM461-02.png
    :width: 400px
    :align: center
    :alt: TM460 外观
    :figclass: align-center

    TM461-E2 外观


测量指标
------------

.. list-table::
   :header-rows: 1

   * - 项目
     - 单位
     - 范围
     - 备注
   * - 测量距离
     - mm
     - 100 ~ 4100
     - 与补光亮度和环境光相关
   * - 深度近视场
     - mm
     - 125 x 95 @ 100
     - H/V: 65°/50°
   * - 深度远视场
     - mm
     - 5225 x 3840 @ 4100
     - H/V: 65°/50° 


Z 方向的距离精度
~~~~~~~~~~~~~~~~~~~~~~~

.. seealso:: 

   1. Z 方向的距离精度：Z 方向上，测得的距离值与距离真值之间的平均偏差。
   2. 以下折线图表示图像质量分别设为 HIGH、MEDIUM、BASIC 时测得的距离精度，横坐标为距离值，纵坐标为距离精度，单位 mm。

.. figure:: ../image/zprecision-tm460-high.png
    :width: 700px
    :align: center
    :alt: Z 方向的距离精度（图像质量 HIGH）
    :figclass: align-center


.. figure:: ../image/zprecision-tm460-medium.png
    :width: 700px
    :align: center
    :alt: Z 方向的距离精度（图像质量 MEDIUM）
    :figclass: align-center


.. figure:: ../image/zprecision-tm460-basic.png 
    :width: 700px
    :align: center
    :alt: Z 方向的距离精度（图像质量 BASIC）
    :figclass: align-center
|


单点抖动
+++++++++++++++++++

.. seealso:: 

   1. 单点抖动：视野内多个像素点深度值在时域上的离散程度。
   2. 以下折线图表示图像质量分别设为 HIGH、MEDIUM、BASIC 时测得的单点抖动分布区间，横坐标为距离值，纵坐标为单点抖动，单位 mm。

.. figure:: ../image/pointprecision-tm460-high.png
    :width: 700px
    :align: center
    :alt: 单点抖动（图像质量 HIGH）
    :figclass: align-center



.. figure:: ../image/pointprecision-tm460-medium.png
    :width: 700px
    :align: center
    :alt: 单点抖动（图像质量 MEDIUM）
    :figclass: align-center



.. figure:: ../image/pointprecision-tm460-basic.png 
    :width: 700px
    :align: center
    :alt: 单点抖动（图像质量 BASIC）
    :figclass: align-center
|


平面度
+++++++++++++++++++

.. seealso:: 

   1. 平面度：视野内多个像素点相对于理想平面的离散程度。
   2. 以下折线图表示图像质量分别设为 HIGH、MEDIUM、BASIC 时测得的平面度分布区间，横坐标为距离值，纵坐标为平面度，单位 mm。

.. figure:: ../image/planarity-tm460-high.png
    :width: 700px
    :align: center
    :alt: 平面度（图像质量 HIGH）
    :figclass: align-center




.. figure:: ../image/planarity-tm460-medium.png
    :width: 700px
    :align: center
    :alt: 平面度（图像质量 MEDIUM）
    :figclass: align-center



.. figure:: ../image/planarity-tm460-basic.png 
    :width: 700px
    :align: center
    :alt: 平面度（图像质量 BASIC）
    :figclass: align-center
|


.. TM461-E2-reuse

图像参数
------------

.. table::
 :width: 695px

 +---------------+------------+-----------------------------------------------+
 |  项目         |    分辨率  |支持的图像质量（帧率）                         |
 +===============+============+===============================================+
 |               |  640*480   |HIGH（7fps）、MEDIUM（15fps）、BASIC（30fps ） |
 +               +------------+                                               +
 |    深度图     |   320*240  |                                               |
 +               +------------+                                               +
 |               |  160*120   |                                               |
 +---------------+------------+-----------------------------------------------+


.. table::
 :width: 695px

 +---------------+------------+---------------+
 |  项目         |    分辨率  |     帧率      |
 +===============+============+===============+
 |               |  1920*1080 |   29fps       |
 +               +------------+---------------+
 |    彩色图     |   1280*720 |   29fps       |
 +               +------------+---------------+
 |               |   640*360  |  29fps        |
 +---------------+------------+---------------+

.. important ::

    #. 彩色图可以与深度图实现点对点对齐，详情请参考示例程序 SimpleView_Registration 或者查看 API 指南。
    #. 通过设置以下属性，可优化 ToF 相机深度图。

       - 图像质量
       - 飞点滤波
       - 调制频道：TM461-E2 支持 5 个调制频道（Channel 0 ~ 4）
       - 激光调制光强
       - 抖动过滤
     
       关于属性说明，详情请查看 API 指南的 :ref:`ToF 相机属性 <tof-feature-label>`。


接口说明
--------

**网络接口**

该相机数据传输采用千兆以太网，网络接口采用 RJ45 接口。为确保网络连接稳定，建议使用接头带固定螺丝的千兆网线。


**电源接口**

该相机的电源接口采用 6 芯推拉自锁航插头，如下图所示。

.. figure:: ../image1/M8-6pin-connector-h.svg
    :width: 180px
    :align: center
    :alt: 电源接口
    :figclass: align-center

    电源接口


.. list-table:: 电源接口引脚说明
   :width: 695px
   :header-rows: 1

   * - 序号
     - 名称
     - 功能描述
     - 配套线芯颜色
   * - 1
     - P_24V
     - 电源正
     - 红色
   * - 2
     - NC
     - 空
     - 黄色
   * - 3
     - NC
     - 空
     - 蓝色
   * - 4
     - NC
     - 空
     - 绿色
   * - 5
     - NC
     - 空
     - 白色
   * - 6
     - P_GND
     - 电源地
     - 黑色


**指示灯**

.. list-table:: 指示灯说明
   :width: 695px
   :header-rows: 1

   * - 颜色
     - 名称
     - 功能描述
   * - 红色
     - 相机状态指示灯
     - 1Hz 缓慢闪烁表示工作正常


电源参数
----------

该相机支持以下两种供电方式：

- PoE 供电
   
  使用 Power Over Ehernet(PoE) 供电，将网线插入 RJ45 插座即可。请使用符合 IEEE802.3at/af 标准的 PoE 为相机供电。

- 外部直流供电
 
  将外部直流电源通过工业航插线缆连接到电源接口，即可为相机供电。供电电压为 24 V，建议使用 24 V 直流电源供电。
  
.. Note::

   外部直流电源和 PoE 供电同时存在时，相机优先选用外部直流电源供电。若此时拔出外部直流电源，相机会切换到 PoE 供电，有可能会重启相机。

.. list-table:: 电源电气指标
   :header-rows: 1

   * - 项目
     - 单位
     - 最小值
     - 典型值
     - 最大值
     - 备注
   * - VCC for Power
     - V
     - 22.8
     - 24
     - 25.2
     - —
   * - P\ :sub:`idle`\
     - W
     - —
     - 2.0
     - —
     - 空闲模式下功耗
   * - P\ :sub:`work`\
     - W
     - —
     - 3.2
     - —
     - 连续工作模式下功耗

物理指标
---------

.. list-table::
   :header-rows: 1

   * - 项目
     - 单位
     - 最小值
     - 典型值
     - 最大值
   * - 宽 x 高 x 深（不含接口）
     - mm
     - —
     - 96.4 x 67.5 x 35.8 
     - —
   * - 重量
     - g
     - —
     - 315
     - —
   * - 工作温度
     - ℃
     - 0
     - —
     - 45
   * - 存储温度
     - ℃
     - -10
     - —
     - 55
   * - 防水防尘
     - IEC 60529
     - 
     - IP50
     - 

.. note::

    防溅、抗水、防尘功能并非永久有效，防护性能可能会因日常磨损而下降。因浸入液体而导致的损坏不在保修范围之内。

    
机械尺寸
---------


.. figure:: ../image/TM461-E2-6S.svg
    :width: 700px
    :align: center
    :alt: 相机机械尺寸图
    :figclass: align-center

    相机机械尺寸图

