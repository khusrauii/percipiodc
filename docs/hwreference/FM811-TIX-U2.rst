.. _FM811-TIX-label:

FM811-TIX
============


.. figure:: ../image/FM811-IX-U2-a.png
    :width: 480px
    :align: center
    :alt: FM811-TIX外观
    :figclass: align-center

    FM811-TIX 前视图

.. figure:: ../image/FM810-TIX-U2-b.png
    :width: 480px
    :align: center
    :alt: FM811-TIX外观
    :figclass: align-center

    FM811-TIX 后视图

测量指标
------------

.. list-table::
   :header-rows: 1

   * - 项目
     - 单位
     - 范围
     - 备注
   * - 测量距离
     - mm
     - 700 ~ 3400
     - 与补光和环境光相关
   * - 深度近视场
     - mm
     - 720 x 610 @ 700
     - H/V：约 54°/47°
   * - 深度远视场
     - mm
     - 3710 x 2890 @ 3400
     - H/V：约 57°/47°   
   * - 精度误差 Z
     - mm
     - 4 @ 1000；  3 @ 1500；  4 @ 2500
     - 与距离呈非线性关系
   * - 精度误差 X/Y
     - mm
     - 4 @ 1000；  5 @ 1500；  8 @ 2500
     - 与距离呈非线性关系

.. include:: FM810-TIX-U2.rst 
   :start-after: FM810-TIX-U2-reuse



