
Percipio Viewer 用户指南
============================    



Percipio Viewer 是图漾基于 Percipio Camport SDK 开发的一款看图软件，可实时预览相机输出的深度图、彩色图、红外图和点云图。

Percipio Viewer 分 Windows 版和 Linux 版，推荐使用 Windows 版。下文以 Percipio Viewer v2.3.0 （Windows 版）为例，介绍其界面和功能。

.. figure:: image/percipioviewer_interface.png
   :width: 700px
   :align: center
   :alt: Percipio Viewer 软件界面
   :figclass: align-center

   Percipio Viewer 软件界面

1. Add Source：用于选择并打开相机、加载本地录像、刷新设备列表。
2. 视图切换区：用于切换 2D、3D、Registration 和 Color 3D 视图。
3. 控制面板区：用于查看信息、设置工作模式、开启/关闭数据流、设置组件参数和录制视频。   
   
   * |on| 表示数据流处于开启状态。
   * |off| 表示数据流处于关闭状态。
4. 图像显示区：用于显示深度图、彩色图、红外图和点云图。
5. Log 显示区：用于显示 log 信息。

.. |on| image:: image/icon_stream_on.png 
.. |off| image:: image/icon_stream_off.png
 

快速开始
---------------

该部分内容主要介绍 Percipio Viewer 的使用流程：

1. :ref:`选择相机 <viewer:选择相机>`
2. :ref:`查看信息 <viewer:查看信息>`
3. :ref:`设置工作模式 <viewer:设置工作模式>`
4. :ref:`预览图像 <viewer:预览图像>`
5. :ref:`保存图像 <viewer:保存图像>`
6. :ref:`录制视频 <viewer:录制视频>`

使用 Percipio Viewer 前，请先：

1. 在 `图漾科技官网下载区 <https://www.percipio.xyz/downloadcenter/>`_ 下载，解压文件后可得到 ``percipio-viewer-2.3.0.exe``。

2. 将图漾相机与上位机连接。连接方式请参见 :ref:`硬件连接 <hardware-connection-label>`。


选择相机
~~~~~~~~~~~~~~~~~~

Percipio Viewer 自 V1.2.3 起，打开后需手动选择并打开相机。

以序列号为 207000106916 的相机为例，按照以下步骤，选择相机：

1. 双击打开可执行文件 ``percipio-viewer-2.3.0.exe``。
   
2. 待相机初始化完成后，点击 :guilabel:`Add Source`，在下拉菜单中点击 :guilabel:`Refresh Source` 刷新设备列表。

3. 在设备列表中选择目标相机的序列号 **207000106916**。

.. figure:: image/select_camera.gif
   :width: 700px
   :align: center
   :alt: 选择相机
   :figclass: align-center

   选择相机

.. note:: 

   若设备列表中的序列号后显示了错误码，表明该相机不能正常打开。关于错误码信息，详情请查看在线文档 :doc:`API 指南 </apiguides/index>`。


查看信息
~~~~~~~~~

点击 |info|，可查看相机信息，包括接口名称、序列号、型号、Mac/IP 地址（网络相机特有）、固件版本和配置版本。

.. figure:: image/camera_detail_info_internet.png
   :width: 320px
   :align: center
   :alt: 网络相机信息
   :figclass: align-center

   网络相机信息

.. figure:: image/camera_detail_info_usb.png
   :width: 320px
   :align: center
   :alt: USB 相机信息
   :figclass: align-center

   USB 相机信息

.. |info| image:: image/icon_info.png

.. note:: 

   固件版本信息较长，当鼠标停留在 **Firmware Version** 信息上时即可显示完整的内容。

设置工作模式
~~~~~~~~~~~~

Percipio Viewer 支持设置相机的工作模式，相机根据设置采集图像。

在所有数据流处于 |off| 时，点击 |more|，并根据下表选择工作模式。


.. list-table::
   :header-rows: 1

   * - 工作模式
     - 相机工作状态
   * - Trigger Mode Off 自由采集模式
     - 开启数据流，相机以最高帧率连续采集图像。
   * - Hardware Trigger Mode 硬触发模式
     - 将外部硬触发信号源与相机连接，开启数据流，相机根据触发信号频率采集图像。
   * - Software Trigger Mode (continue) 连续软触发模式
     - 开启数据流，相机接收到软件触发信号后采集图像。
   * - Software Trigger Mode (single) 单帧软触发模式
     - 开启数据流，点击 |trigger|，每点击一次，相机采集一帧图像。

.. |more| image:: image/icon_more.png
.. |trigger| image:: image/icon_trigger.png

选中的工作模式右侧会带有图标 **√**。

.. figure:: image/selected_mode.png
   :width: 480px
   :align: center
   :alt: 选中的工作模式
   :figclass: align-center

   选中的工作模式



预览图像
~~~~~~~~~

Percipio Viewer 支持预览深度图、彩色图、左右红外图、点云图以及调试组件参数。

按照以下步骤，预览图像： 

1. 打开数据流，即可在图像显示区实时预览相应的图像。

   图像上方的数据表示相机输出图像的帧率。

   .. figure:: image/show_fps.png
      :width: 700px
      :align: center
      :alt: 深度图、彩色图、左/右红外图
      :figclass: align-center
      
      深度图、彩色图、左/右红外图
     
   .. |right| image:: image/icon_right.png  

2. 点击数据流左侧 |right| 展开页面，可根据实际情况调试参数。

  .. note::

 	  1. 部分参数需在数据流处于 |off| 时修改。建议参考 Log 信息显示区的提示操作。
 	  2. 各个型号的相机支持设置的参数不同，Percipio Viewer 界面的显示随之改变。下表以序列号为 **207000106916** 的相机为例介绍参数。
    
  .. figure:: image/para_207000106916.png
     :width: 700px
     :align: center
     :alt: 207000106916相机的参数界面
     :figclass: align-center
     
     207000106916相机的参数界面



  .. dropdown:: 参数说明
      :open:
      :animate: fade-in-slide-down
          
      **Depth Stream** 

      +----------------------+-----------------+-----------------------------------------------------------------------------------+
      | 类别                 |    参数         |    说明                                                                           |
      +======================+=================+===================================================================================+
      | —                    |  Resolution     |  深度图的分辨率。                                                                 |        
      +----------------------+-----------------+-----------------------------------------------------------------------------------+
      | Control              | Power           | 激光强度。                                                                        |
      +----------------------+-----------------+-----------------------------------------------------------------------------------+
      | Depth Visualization  | Color Scheme    | 深度图的渲染方式。                                                                |
      +----------------------+-----------------+-----------------------------------------------------------------------------------+
      | Post-Processing      | Fill Hole       | 填洞，详情请参考 :ref:`Fill Hole填洞功能 <viewer:Fill Hole 填洞功能>`。           |
      +                      +-----------------+-----------------------------------------------------------------------------------+
      |                      | Remove Outlier  | 降噪，详情请参考 :ref:`Remove Outlier降噪功能 <viewer:Remove Outlier 降噪功能>`。 |
      +----------------------+-----------------+-----------------------------------------------------------------------------------+

          
      **Color Stream** 

      +----------------------+--------------------+---------------------------------------+
      | 类别                 |    参数            |    说明                               |
      +======================+====================+=======================================+
      | —                    |  Resolution        |  彩色图的分辨率。                     |        
      +----------------------+--------------------+---------------------------------------+
      |                      | analog gain        | 模拟增益。                            |
      +                      +--------------------+---------------------------------------+
      |                      | r/g/b gain         | R/G/B增益。                           |
      +                      +--------------------+---------------------------------------+
      | Control              | exposure time      | 曝光时间。                            |
      +                      +--------------------+---------------------------------------+
      |                      | auto exposure      | 自动曝光。部分相机支持。              |
      +                      +--------------------+---------------------------------------+
      |                      | auto gain ctrl     | 自动增益。部分相机支持。              |
      +                      +--------------------+---------------------------------------+
      |                      | auto white balance | 自动白平衡。 部分相机支持。           |
      +----------------------+--------------------+---------------------------------------+
      | Post-Processing      | Auto ISP           | 软件ISP。部分相机支持。               |
      +                      +--------------------+---------------------------------------+
      |                      | Undistort EN       | 使能畸变矫正。部分相机支持。          |
      +----------------------+--------------------+---------------------------------------+

      **Left IR / Right IR Stream** 

      +----------------------+-----------------+---------------------------------------+
      | 类别                 |    参数         |    说明                               |
      +======================+=================+=======================================+
      |  Control             | Gain            | 增益。                                |
      +                      +-----------------+---------------------------------------+
      |                      | Power           | 激光强度。                            |
      +----------------------+-----------------+---------------------------------------+
 
  
3. 点击视图切换区的 :guilabel:`2D`、 :guilabel:`3D`、 :guilabel:`Registration` 和 :guilabel:`Color 3D`，可切换视图。
  
   .. note::
    
      只有在 Depth Stream 和 Color Stream 同时打开时，才可切换至 Registration 或 Color 3D 视图。

4. 将鼠标置于 2D/3D 图上并选择以下方式调整视图：
   
   * 缩放图像：滚动鼠标滚轮
   * 平移图像：按住鼠标滚轮并拖动
   * 旋转图像（仅 3D/Color 3D 视图支持）：按住鼠标左键并拖动

   .. note::

    3D/Color 3D 视图下，点击 |reset|，可复位视图。

    .. |reset| image:: image/reset.png


保存图像
~~~~~~~~~

按照以下步骤，保存 2D/3D 图：

.. note:: 
    
    图像文件默认保存在 C 盘，若无法保存到默认路径，请参考 ::ref:`无法保存文件至 C 盘，怎么办？ <viewer:无法保存文件至 C 盘，怎么办？>`。

* 切换至 2D 视图，点击 |2D| ，选择保存路径并点击保存。

   .. |2D| image:: image/icon_save_2D.png
   .. |3D| image:: image/icon_save_3D.png

* 切换至 3D/Color 3D 视图，点击 |3D| ，选择 3D 点云图导出的文件格式（支持 ``.xyz`` 和 ``.ply`` 格式）和保存路径并点击保存。


录制视频
~~~~~~~~~~~~~~~~~

Percipio Viewer 支持录制相机采集图像时的视频。录制视频过程中，支持调整部分参数，不支持关闭/开启数据流。

按照以下步骤，录制视频：

1. 点击 |record|，并设置 ``.bag`` 文件的保存路径。视频录制开始。

   .. |record| image:: image/icon_record.png

   .. note:: 

     ``.bag`` 文件默认保存在 C 盘，若无法保存到默认路径，请参考 :ref:`无法保存文件至 C 盘，怎么办？ <viewer:无法保存文件至 C 盘，怎么办？>`。

   .. figure:: image/record_interface.png
      :align: center
      :alt: 录制视频
      :figclass: align-center

      录制视频

2. 点击 |stop|。视频录制结束。                

   .. |stop| image:: image/icon_stop.png

   ``.bag`` 文件保存路径会显示在图像显示区右上角和 Log 信息显示区。 


若需回放录制视频，点击 :guilabel:`Add Source`，在下拉菜单中点击 :guilabel:`Load Recorded Sequence`，并选择目标 ``.bag`` 文件。

.. figure:: image/load_recorded_sequence.png
   :width: 320px
   :align: center
   :alt: 录像回放
   :figclass: align-center
   
   录像回放



特色功能
---------------

该部分内容主要介绍 Percipio Viewer 的特色功能：

* :ref:`对齐功能 <viewer:对齐功能>`
* :ref:`深度优化 <viewer:深度优化>`
* :ref:`泛光功能 <viewer:泛光功能>`
* :ref:`HDR 功能 <viewer:HDR 功能>`

对齐功能
~~~~~~~~~~~~~~~~~

Percipio Viewer 支持 RGB-Depth 对齐和 RGB-点云对齐。

RGB-Depth 对齐
++++++++++++++++

在 Depth Stream 和 Color Stream 处于 |on| 时，点击 :guilabel:`Registration`，可切换到彩色图与深度图对齐的视图。

.. figure:: image/rgbd.png
   :width: 700px
   :align: center
   :alt: 彩色图和深度图对齐
   :figclass: align-center
   
   彩色图和深度图对齐


.. tip:: 

    使用 RGB-Depth 对齐时，Left IR Stream 和 Right IR Stream 自动关闭。



RGB-点云对齐
++++++++++++++++

在 Depth Stream 和 Color Stream 处于 |on| 时，点击 :guilabel:`Color 3D`，可切换到彩色图与点云图对齐的视图。

.. figure:: image/rgb3D.png
   :width: 700px
   :align: center
   :alt: 彩色图和点云图对齐
   :figclass: align-center
   
   彩色图和点云图对齐


.. tip:: 

    使用 RGB-Depth 对齐时，Left IR Stream 和 Right IR Stream 自动关闭。



深度优化
~~~~~~~~~~~~~~~~~

通过以下功能，可优化的深度图质量：

* :ref:`Fill Hole 填洞功能 <viewer:Fill Hole 填洞功能>`
* :ref:`Remove Outlier 降噪功能 <viewer:Remove Outlier 降噪功能>`
* :ref:`Depth Quality 深度质量 <viewer:Depth Quality 深度质量>`

Fill Hole 填洞功能
++++++++++++++++++++

Fill Hole 填洞功能可填补深度图上缺失的数据。该功能结合 Remove Outlier 降噪功能一起使用，能获得成图效果最佳的深度图。

在 Depth Stream 处于 |on| 时，选中 :guilabel:`Fill Hole`，并根据深度图调节参数 **kSize** 和 **hSize**。

.. note:: 

    若 kSize 和 hSize 的参数值设置太大，可能会出现深度图失真的现象。

使用 Fill Hole 填洞功能前后对比：

.. figure:: image/fill_hole_before.png 
   :width: 700px
   :align: center
   :alt: 使用 Fill Hole 填洞功能前
   :figclass: align-center
   
   使用 Fill Hole 填洞功能前
       
.. figure:: image/fill_hole_after.png 
   :width: 700px
   :align: center
   :alt: 使用 Fill Hole 填洞功能后  
   :figclass: align-center
   
   使用 Fill Hole 填洞功能后


Remove Outlier 降噪功能
++++++++++++++++++++++++

Remove Outlier 降噪功能可减少深度图中的噪点，优化深度图成像效果。

在 Depth Stream 处于 |on| 时，选中 :guilabel:`Remove Outlier`，并根据深度图调节参数 **spkSize**。

使用 Remove Outlier 降噪功能前后对比：

.. figure:: image/remove_outlier_before.png
   :align: center
   :alt: 使用 Remove Outlier 降噪功能前
   :figclass: align-center
   
   使用 Remove Outlier 降噪功能前

.. figure:: image/remove_outlier_after.png 
   :align: center
   :alt: 使用 Remove Outlier 降噪功能后  
   :figclass: align-center
   
   使用 Remove Outlier 降噪功能后


Depth Quality 深度质量
++++++++++++++++++++++++++++++++++++++++

Depth Quality 深度质量用于设置相机输出的深度图质量，为适应不同应用的需求。

.. note:: 

    深度质量是 ToF 相机 TM460-E2 特有的功能，只有用 Percipio Viewer 打开了该型号的相机，才可在界面中设置深度质量。

在所有数据流处于 |off| 时，设置深度图的 :guilabel:`depth quality`，并打开 **Depth Stream**。

* depth quality basic：深度值抖动幅度大，输出帧率高。 
* depth quality medium：深度值抖动幅度中等，输出帧率中等。 
* depth quality high：深度值抖动幅度小，输出帧率低。

.. figure:: image/depth_quality_basic.png
   :width: 700px
   :align: center
   :alt: depth quality basic
   :figclass: align-center
   
   depth quality basic 

.. figure:: image/depth_quality_medium.png
   :width: 700px
   :align: center
   :alt: depth quality medium
   :figclass: align-center
   
   depth quality medium 

.. figure:: image/depth_quality_high.png
   :width: 700px
   :align: center
   :alt: depth quality high
   :figclass: align-center
   
   depth quality high


泛光功能
~~~~~~~~~~~~~~~~~

泛光功能用于辅助相机在线动态标定。

.. note:: 

    泛光功能是 PS801-E1 特有的功能，只有用 Percipio Viewer 打开了该型号的相机，才可在界面中设置泛光功能。


在 Left IR Stream 和 Right IR Stream 处于 |on| 时，选中 :guilabel:`flash light enable`，并根据实际需求设置泛光灯亮度 **flash light intensity**。
   
.. figure:: image/flash.png
   :width: 700px
   :align: center
   :alt: 设置泛光功能
   :figclass: align-center
   
   设置泛光功能



HDR 功能
~~~~~~~~~~~~~~~~~

HDR（高动态范围）功能用于优化高对比度场景的深度成像效果，设置完 HDR 参数后需要调节 Left/Right IR 的曝光时间，以获得成像效果最佳的深度图。

.. note:: 

    HDR 功能是 PS801-E1 特有的功能，只有用 Percipio Viewer 打开了该型号的相机，才可在界面中设置 HDR 功能。

按照以下步骤，设置 HDR 功能： 

1. 在 Left IR Stream 和 Right IR Stream 处于 |on| 时，选中 :guilabel:`HDR`，设置 **参数 1** 和 **参数 2**，并按 :guilabel:`Enter` 键确认设置。
   
   .. tip:: 

      参数 1 和参数 2 的设置范围 0，1，2。

   .. figure:: image/parameter_1_2.png
      :width: 320px
      :align: center
      :alt: 设置参数1和参数2
      :figclass: align-center
   
      设置参数1和参数2

2. 设置 Left IR 和 Right IR 的曝光时间 **exposure time**，获得最佳的深度图。

使用 HDR 功能前后对比：

.. figure:: image/disable_hdr.png
   :width: 700px
   :align: center
   :alt: 使用 HDR 功能
   :figclass: align-center
   
   使用 HDR 功能前

.. figure:: image/enable_hdr.png
   :width: 700px
   :align: center
   :alt: 使用 HDR 功能后
   :figclass: align-center
   
   使用 HDR 功能后


.. _viewer-faq-label:

常见问题
---------------

Percipio Viewer 支持哪些操作系统？
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Percipio Viewer 支持以下操作系统： 

*  Windows：10/11
*  Linux：Ubuntu 16.04/18.04/20.04

无法保存文件至 C 盘，怎么办？
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**问题描述**

保存 2D 图、3D 图和录像文件至 C 盘时，出现报错。

.. figure:: image/report_err_dialog.png
   :width: 480px
   :align: center
   :alt: 报错信息
   :figclass: align-center
   
   报错信息



**分析思路** 

电脑可能已开启了“勒索软件防护”，限制了部分应用程序对电脑上的文件、文件夹和内存区域进行未授权更改。 

**解决方法** 

在电脑上搜索 “ **勒索软件防护** ”，关闭 **受控制文件夹的访问**。

.. figure:: image/close_visit.png
   :align: center
   :alt: 关闭受控制文件夹的访问
   :figclass: align-center
   
   关闭受控制文件夹的访问


