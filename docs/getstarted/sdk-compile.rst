
.. _software-preparation-label:

SDK 下载与编译
--------------------

该部分旨在指导用户搭建 Percipio 深度相机应用开发的软硬件环境以及应用 Percipio 深度相机的软件开发包 PercipioDC Camport SDK（以下简称 Camport SDK）。

Camport SDK 适用于 Windows、Linux(OpenNI2)、Android 和 ROS 平台，并为开发者提供了一系列友好的 API 和简单的示例程序。用户可以基于 Camport SDK，获取 :ref:`深度图 <depth-image-label>`、:ref:`红外图 <ir-image-label>`、:ref:`彩色图 <color-image-label>` 和 :ref:`点云图 <point3D-image-label>`，用于导航、避障、三维建模、手势识别等应用。

.. _build-pc-environment-label:

上位机环境 (C++)
~~~~~~~~~~~~~~~~~~~~~

若需在上位机运行图像应用软件或开发上位机图像应用软件，请先配置上位机环境，包括下载 Camport SDK、配置、编译和运行。

Camport SDK 应用开发环境配置如下图所示：

.. figure:: ../image/envbuildingflow1.png
    :align: center
    :alt: Camport SDK开发环境搭建
    :figclass: align-center

    Camport SDK 应用开发环境配置


Linux平台
+++++++++++++

Linux SDK 支持 AArch64、ARMv7hf、RaspberryPi、i686 和 x86-64 硬件平台，并为开发者编译程序提供相应平台的库文件。Linux SDK 未提供可执行的应用程序，用户可通过编译示例代码来生成可执行的应用。

下面以 Ubuntu14.04 为例介绍在 Linux 平台为 Percipio 深度相机搭建开发环境的过程：

1. :ref:`下载 Camport3 SDK <download-sdk-linux-label>`

2. :ref:`安装 USB 驱动 <usb-driver-linux-label>`

3. :ref:`安装依赖 <install-dependence-linux-label>`

4. :ref:`设置权限 <acesspermission-label>`

5. :ref:`编译 <code-linux-label>`

6. :ref:`运行 <run-linux-label>`

.. _download-sdk-linux-label:

下载 Camport3 SDK
*******************

选择以下任意方式，下载 Camport3 SDK：

* 使用浏览器访问 http://github.com/percipioxyz/ 并下载 Camport3 SDK。

* 使用 git 指令获取本地副本：打开终端，切换到需存放 SDK 的工作目录下，输入 git clone 命令克隆远程仓库。
   
  :: 

     git clone https://github.com/percipioxyz/camport3.git 

Camport3 SDK 目录结构如下图所示：

.. figure:: ../image/camport3folder.png
    :width: 480px
    :align: center
    :alt: Camport3 SDK目录结构
    :figclass: align-center

    Camport3 SDK目录结构


**Doc 目录**

存放了 SDK API 参考文档。该文档是通过 Doxygen 生成的 PDF 文件，包括了类、结构和接口的描述信息。

**include 目录**

存放了用于调用动态库的头文件：

* ``TYImageProc.h``：图像后处理函数接口的头文件。

* ``TYCoordinateMapper.h``：图像空间转换的映射。

* ``TYApi.h`` 文件：提供了用于配置深度相机、获取图像数据的全部 API。用户可以基于这些 API 开发自己的应用。

**lib/linux 目录**

存放了支持 Linux 操作系统的库文件，兼容 AArch64、ARMv7hf、RaspberryPi、i686 和 x86-64 硬件平台。

**lib/win 目录**

存放了支持 Windows 操作系统的库文件，兼容 x64 和 x86 硬件平台。

**sample 目录**

存放了可供用户编程参考的示例程序。关于示例程序，详情请参见 :ref:`示例程序说明 <sample-exe-label>`。


.. _usb-driver-linux-label:

安装 USB 驱动
*******************

Camport SDK 默认使用 LibUSB 作为 USB 深度相机的用户态驱动库。使用 Percipio USB 深度相机前需要安装 LibUSB 软件。 ::

   sudo apt-get install libusb-1.0-0-dev






.. _install-dependence-linux-label:

安装依赖
*******************

**CMake**

CMake 是一个跨平台的安装（编译）工具，可以用简单的语句来描述所有平台的安装（编译过程）。它能够输出各种各样的 makefile 或者 project 文件，能测试编译器所支持的 C++ 特性，类似 UNIX 下的 automake。只是 CMake 的组态档取名为 CMakeLists.txt。CMake 并不直接建构出最终的软件，而是产生标准的建构档（如 UNIX 的 Makefile 或 Windows Visual C++ 的 projects / workspaces），然后再以一般的建构方式使用。

Camport SDK 使用 CMake 来构建标准的工程，利用各平台的原生建构系统的能力，可适配不同的平台环境。 ::

   sudo apt-get install cmake

.. note::
   
   Camport SDK 要求用户安装 CMake 2.8.0 及更高的版本。


**OpenCV**

Camport SDK 可通过 USB 接口或者以太网接口接收 Percipio 深度相机输出的深度数据。取得深度数据后，Camport SDK 中提供的示例程序使用 OpenCV 图像处理软件库进行深度数据的渲染。编译示例程序前，需要安装 OpenCV 图形处理软件库。 ::

   sudo apt-get install libopencv-dev


.. note::

   1. Camport SDK 要求用户安装 OpenCV 2.4.8 及更高版本。
   
   2. 如果目标应用程序不使用 OpenCV 进行图像显示或者其他图像处理计算，则无需安装 OpenCV。


.. _acesspermission-label:

设置权限
*******************

根据 Linux 系统的权限管理机制，默认情况下需要 root 权限才能访问 Percipio **USB 深度相机**，非 root 用户可以创建 udev rule 来修改设备权限。

在 ``etc/udev/rules.d`` 文件所在目录下，新建一个扩展名为 ``.rules`` 的规则文件（如 88-tyusb.rules），并把用户加入该文件中指定的组（如:tofu），重启 PC 后获得 USB 深度相机的访问权限。规则文件内容如下： ::

  SUBSYSTEM== "usb",ATTRS{idProduct}=="1003",ATTRS{idVendor}=="04b4",GROUP="tofu",MODE="0666" 

.. note::

    规则文件名的开头须为数字 (0 ~ 99)，数字越大，优先级越高。访问 Percipio **网络深度相机** 不需要 root 权限。


.. _code-linux-label:

编译
*******************

进入 camport3 目录，执行以下命令编译示例代码。 ::

  sudo cp lib/linux/lib_x64/libtycam.so* /usr/lib/
  cd sample
  mkdir build
  cd build
  cmake ..
  make

在 ``camport3/sample/build/bin`` 目录下生成若干编译生成的可执行文件。

.. _run-linux-label:

运行
*********************

深度相机连接电脑后开始系统初始化，直到相机背面 Power 灯以 1Hz 的频率闪烁。相机初始化后，以 root 权限运行以下命令，即可接收并显示深度图像。 ::

  sudo ./SimpleView_FetchFrame

.. note::

    运行应用程序后，如果提示 "Couldn't open device, some information will be missing, ret: -3"，请检查系统的访问权限配置是否正确。


Windows 平台
++++++++++++++++++

Windows SDK 支持 X86（32位）和 X64（64位）硬件平台，并提供支持 Window7 及以上版本操作系统的驱动文件。Windows SDK 包括一些直接可以执行的应用程序（例如查看深度图）和一些应用程序的示例代码。

下面以 Windows10 X64 位系统为例介绍在 Windows 平台为 Percipio 深度相机搭建开发环境的过程：

1. :ref:`下载 Camport3 SDK <download-sdk-windows-label>`

2. :ref:`安装 USB 驱动 <install-usb-drive-label>`

3. :ref:`安装依赖 <install-dependence-windows-label>`

4. :ref:`编译 <code-windows-label>`


.. _download-sdk-windows-label:


下载 Camport3 SDK
*******************

选择以下任意方式，下载 Camport3 SDK：

* 使用浏览器访问 http://github.com/percipioxyz/ 并下载 Camport3 SDK。

* 使用 git 指令获取本地副本：打开终端，切换到需存放 SDK 的工作目录下，输入 git clone 命令克隆远程仓库。
   
   ::
      
      git clone https://github.com/percipioxyz/camport3.git

Camport3 SDK 目录结构如下图所示：

.. figure:: ../image/camport3-win-folder.png
    :width: 480px
    :align: center
    :alt: Camport3 SDK目录结构
    :figclass: align-center

    Camport3 SDK 目录结构

**Doc 目录**

存放了 SDK API 参考文档。该文档是通过 Doxygen 生成的 ``PDF`` 文件，包括了类、结构和接口的描述信息。


**include 目录**

存放了用于调用动态库的头文件：

* ``TYImageProc.h``：图像后处理函数接口的头文件。

* ``TYCoordinateMapper.h``：图像空间转换的映射。

* ``TY_API.h`` 文件：提供了用于配置深度相机、获取图像数据的全部 API，用户可以基于这些 API 开发自己的应用。

**lib/linux 目录**

存放了支持 Linux 操作系统的库文件，兼容 AArch64、ARMv7hf、RaspberryPi、i686 和 x86-64 硬件平台。

**lib/win 目录**

存放了支持 Windows 操作系统的库文件，兼容 x64 和 x86 硬件平台。

**sample 目录**

存放了可供用户编程参考的示例程序。关于示例程序，详情请参见 :ref:`示例程序说明 <sample-exe-label>`。

.. _install-usb-drive-label:

安装 USB 驱动
*******************

1. 使用 USB 线把 Percipio 深度相机与 Windows PC 连接后，右键点击系统桌面左下角的 :guilabel:`开始`，并在开始菜单中点击 :guilabel:`设备管理器`，设备管理器中可发现未识别的 USB 设备 ``PERCIPIO DEVICEEE``，如下图所示：

.. figure:: ../image/unknownusbdevice.png
    :width: 480px
    :align: center
    :alt: 未识别 USB 设备
    :figclass: align-center

    未识别 USB 设备

2. 右击该设备并选择 :guilabel:`更新驱动程序`。根据 PC 的 Windows 版本，选择 Camport3 软件开发包中 ``lib/win/driver`` 目录下的驱动，按照系统提示完成驱动安装。

.. figure:: ../image/camportdriver.png
    :width: 480px
    :align: center
    :alt: Camport3设备驱动
    :figclass: align-center

    Camport3设备驱动

驱动安装成功后，可以在设备管理器中发现 Percipio Device 设备。

.. figure:: ../image/percipiodevice.png
    :width: 480px
    :align: center
    :alt: Percipio Device 设备
    :figclass: align-center

    Percipio Device 设备


.. _install-dependence-windows-label:

安装依赖
*******************

**CMake**

CMake 是一个跨平台的安装（编译）工具，可以用简单的语句来描述所有平台的安装（编译过程）。它能够输出各种各样的 makefile 或者 project 文件，能测试编译器所支持的 C++ 特性，类似 UNIX 下的 automake。只是 CMake 的组态档取名为 CMakeLists.txt。CMake 并不直接建构出最终的软件，而是产生标准的建构档（如 UNIX 的 Makefile 或 Windows Visual C++ 的 projects / workspaces），然后再以一般的建构方式使用。

Camport SDK 使用 CMake 来构建标准的工程，利用各平台的原生建构系统的能力，可适配不同的平台环境。

.. note::
   
   Camport SDK 要求用户安装 CMake 2.8.0 及更高的版本。

**OpenCV**

Camport SDK 可通过 USB 接口或者以太网接口接收 Percipio 深度相机输出的深度数据。取得深度数据后，Camport SDK 中提供的示例程序使用 OpenCV 图像处理软件库进行深度数据的渲染。编译示例程序前，需要安装 OpenCV 图形处理软件库。

.. note::

   1. Camport SDK 要求用户安装 OpenCV 2.4.8 及更高版本。
   
   2. 如果目标应用程序不使用 OpenCV 进行图像显示或者其他图像处理计算，则无需安装 OpenCV。


.. _code-windows-label:

编译
*******************

编译前，请先：

1. 安装 Visual Studio。

2. 安装 :ref:`依赖 <install-dependence-windows-label>`。

.. note:: 

   1. 安装 Visual Studio 时，需勾选 Visual C++ 库模块一并安装，否则无法编译成功。
   2. 若不会安装 Visual C++ 库，可联系 Percipio 售后支持。

编译步骤如下：

1. 在 SDK sample 目录下创建 ``build`` 目录。

2. 启动 **cmake-gui**。

3. 指定源码目录到 sample，编译输出目录为 ``sample/build``。


   .. figure:: ../image/wincompilestep1.png
       :width: 480px
       :align: center
       :alt: wincompilestep2
       :figclass: align-center


4. 点击 :guilabel:`Configure`，选择对应的 Visual Studio 版本并点击 :guilabel:`Finish`。


   .. figure:: ../image/wincompilestep2.png
       :width: 480px
       :align: center
       :alt: wincompilestep2
       :figclass: align-center

5. 在 OpenCV_DIR 一栏指定到 ``opencv/build`` 文件路径。
   
   .. figure:: ../image/wincompilestep3.png
       :width: 480px
       :align: center
       :alt: wincompilestep3
       :figclass: align-center

6. 在系统环境变量中配置 OpenCV 库的路径。
   
   具体步骤：进入 **环境变量** 对话框，选中 “Path” 一栏并点击 :guilabel:`编辑`，在 **编辑环境变量** 对话框中配置 OpenCV 库的路径。
   
   .. figure:: ../image/wincompilestep5.png
      :width: 480px
      :align: center
      :alt: wincompilestep5
      :figclass: align-center

7. 点击 :guilabel:`Generate`。


    .. figure:: ../image/wincompilestep4.png
       :width: 480px
       :align: center
       :alt: wincompilestep4
       :figclass: align-center


8. 点击 :guilabel:`Open Project`，打开工程。
   
   .. figure:: ../image/wincompilestep6.png
       :width: 480px
       :align: center
       :alt: wincompilestep6
       :figclass: align-center
  
  .. note::
     
     此后可通过 ``camport3/sample/build`` 路径下的 ``Project.sln`` 或 ``ALL_BUILD.vcxproj`` 文件打开工程。

9.  编译工程：在 Visual Studio 菜单栏依次点击 :guilabel:`生成`> :guilabel:`生成解决方法`。

   .. figure:: ../image/wincompilestep7.png
       :width: 480px
       :align: center
       :alt: wincompilestep7
       :figclass: align-center

.. _run-windows-label:

运行
*********************

深度相机连接 PC 后开始系统初始化，直到相机背面 Power 灯以 1Hz 的频率闪烁。在 Windows10 X64 系统中，运行开发包中 ``lib/win/hostapp/x64/SimpleView_FetchFrame.exe`` 可执行文件，即可获得深度图像。
或者将开发包中 ``lib/win/hostapp/x64/tycam.dll`` 上述编译生成的文件夹 ``sample/build/bin/Release`` 中，并运行文件夹内的 ``SimpleView_FetchFrame.exe``，也可获得深度图。


Android 平台
+++++++++++++++++++++++++++

Android SDK 支持 ARMV7 硬件平台和 Android 4.4系统，并提供相应平台的库文件。Android SDK 为用户提供可执行的应用程序，也为开发者提供了一些应用程序的示例代码。

下载 Camport3 Android SDK
**************************************

选择以下任意方式，下载 Camport3 Android SDK：

* 使用浏览器访问 http://github.com/percipioxyz/ 并下载 Camport3 Android SDK。

* 使用 git 指令获取本地副本：打开终端，切换到需存放 SDK 的工作目录下，输入 git clone 命令克隆远程仓库。
  
 ::
   
     git clone https://github.com/percipioxyz/camport3_android.git

Camport3 Android SDK 目录如图所示：

.. figure:: ../image/camportandroidfolder.png
    :width: 480px
    :align: center
    :alt: Camport Android SDK目录
    :figclass: align-center

    Camport Android SDK目录

**lib 目录**

存放了 Android 平台的库文件，支持的 ARM 平台有 arm64-v8a、armeabi-v7a、armeabi，共计 3 类。

**sample 目录**

* ``Simple_FetchFrame``：存放了 Camport-FetchFrame 程序的源代码，可以编译 Console 应用程序或者 apk。

* ``binary_for_arm64-v8a``：存放了可执行的 Console 应用程序和 apk。

* ``binary_for_armeabi-v7a``：存放了可执行的 Console 应用程序和 apk。

* ``binary_for_armeabi``：存放了可执行的 Console 应用程序和 apk。

编译
******

#. 搭建 Android 的开发环境。
#. 下载 Android 版本的 OpenCV SDK。
#. 复制相关代码到开发环境。
#. 开始编译 SDK。

.. note::

  Camport_FetchFrame.apk 基于 OpenCV 开发，编译源码需要 OpenCV Android SDK 配合，具体安装步骤请参考 OpenCV 官网。


运行示例
***********

以 USB 相机为例，介绍如何通过 Android 系统运行相机，操作步骤如下：
  
1. 使用 USB 连接线将 PC 与 Android 设备连接。
2. 通过 adb 复制 ``lib/armeabi/armeabi-v7a/`` 目录下的库文件到 ``/system/lib/`` 下。
3. 进入 SDK 的 ``sample/binary_for_armeabi-v7a`` 目录。
4. 通过 adb 安装 Campor_FetchFrame.apk 和 OpenCV_2.4.9_Manager_2.18_armeabi.apk。
5. 使用 USB 连接线将深度相机与 Android 设备连接。
6. 如需运行 SimpleView_FetchFrame，复制 ``sample/binary_for_ARMV7/SimpleView_FetchFrame`` 到 Android 设备的 ``/data`` 目录，并在命令行执行以下命令即可看到相机输出的深度图像：
   ::

     /data/SimpleView_FetchFrame

.. note::

 根据 Linux 系统的权限管理机制，如果没有 root 权限，不能操作 Percipio USB 深度相机。非 root 用户可以创建 udev rule 来修改设备权限，可参考 :ref:`acesspermission-label` 。Android 平台可以使用此方式进行权限设置，也可以使用 Android API 申请设备权限。

OpenNI 套件
+++++++++++++

OpenNI2 SDK 基于 Linux SDK 开发，提供相应平台的库文件。

OpenNI（开放自然交互）是一个多语言，跨平台的框架，它定义了编写应用程序，并利用其自然交互的 API。OpenNI 的主要目的是要形成一个标准的 API，来搭建视觉和音频传感器与视觉和音频感知中间件通信的桥梁。


下载 Camport3 OpenNI2 SDK
***************************

选择以下任意方式，下载 Camport3 OpenNI2 SDK：

* 使用浏览器访问 http://github.com/percipioxyz/ 并下载 Camport3 OpenNI2 SDK。

* 使用 git 指令获取本地副本：打开终端，切换到需存放 SDK 的工作目录下，输入 git clone 命令克隆远程仓库。
  
  ::
   
     git clone https://github.com/percipioxyz/camport3_openni2.git

Camport3 OpenNI2 SDK 的目录图所示：

.. figure:: ../image/camportopennifolder.png
    :width: 480px
    :align: center
    :alt: Camport OpenNI2 SDK目录
    :figclass: align-center

    Camport3 OpenNI2 SDK 目录

Percipio 提供各个平台的二进制安装包，按照平台配置情况安装相应开发包，即可支持 OpenNI2 开发环境。该开发包根目录下的 Percipio.ini 复制到 ``/etc/openni2`` 目录，Camport3 OpenNI2 SDK 通过读取该文件的配置来设置 Percipio 深度相机输出的深度和彩色图像数据分辨率，相机默认输出的深度和彩色图像数据分辨率是 640x480。


安装 Camport3 OpenNI2 SDK
***************************

以 X86 平台为例，执行以下命令，安装 Camport3 OpenNI2 SDK：
::

   sudo dpkg -i libopenni2-0_2.2.0.33+dfsg-4_i386.deb
   sudo dpkg -i libopenni2-dev_2.2.0.33+dfsg-4_i386.deb

.. note::

  安装后未能通过 OpenNI 接口接收深度图像数据时，可尝试运行开发包内的 SimpleRead_** 应用，检验 Linux 基础环境下的数据通路是否正常。


ROS 平台
+++++++++++++++++++++

ROS（Robot Operating System，下文简称 **ROS**）是一个适用于机器人的开源的元操作系统。它提供了操作系统应有的服务，包括硬件抽象，底层设备控制，常用函数的实现，进程间消息传递，以及包管理。通过 OpenNI2 框架，Percipio 深度相机可充分接入 ROS 系统。

ROS SDK 支持 ROS Indigo 平台，并提供相应平台的库文件。

下载 Camport ROS SDK
**************************

Camport ROS SDK 依赖于 OpenNI2 SDK，在下载 Camport3 ROS SDK 前，需安装 OpenNI2 支持软件包。

选择以下任意方式，下载 Camport3 ROS SDK：

* 使用浏览器访问 http://github.com/percipioxyz/ 并下载 Camport3 ROS SDK。

* 使用 git 指令获取本地副本：打开终端，切换到需存放 SDK 的工作目录下，输入 git clone 命令克隆远程仓库。
  
  ::
   
     git clone https://github.com/percipioxyz/camport_ros.git

Camport ROS SDK 的目录如下图所示：

.. figure:: ../image/camportrosfolder.png
    :width: 480px
    :align: center
    :alt: Camport ROS SDK目录
    :figclass: align-center

    Camport ROS SDK 目录

* ``openni2_camera``：ROS 调用 OpenNI2 的封装层。
* ``openni2_launch``：打开 OpenNI2 设备并获取深度和颜色数据应用。
* ``depthimage_to_laserscan``：打开 OpenNI2 设备并获取线性深度数据应用。

编译
******

在开发包根目录下执行以下命令：
::

  $catkin_make

配置环境变量
**************
::

  $echo "source ~/camport_ros/devel/setup.bash" >> ~/.bashrc
  $source ~/.bashrc

运行
********

* 执行以下命令，在 RVIZ 中查看 Depth Camera：
  ::

     $roscore
     $roslaunch openni2_launch openni2.launch
     $rosrun rviz rviz

  在 RVIZ 的 ``/camera/image/depth`` 目录下添加深度图像视图，可查看三维空间视图。


* 执行如下命令，在 RVIZ 中查看从深度图中仿真出的 Laser Scan 图：
  ::

     $roscore
     $roslaunch depthimage_to_laserscan depthimage_to_laserscan.launch
     $rosrun rviz rviz

  在 RVIZ 的 ``/scan/LaserScan`` 目录下添加激光扫描视图，可查看模拟线激光视图。


多设备同时接入方案
**************************

参考 ``openni2.launch`` 文件，创建多个 ``.launch``。每个 ``.launch`` 文件中 “camera” 和 “device_id” 配置成不同值，其中 “device_id” 的值为设备序列号。例如，要同时运行两个设备，创建两个 ``.launch`` 文件 ``openni2_camera1.launch`` 和 ``openni2_camera2.launch``。

``openni2_camera1.launch`` 内容如下：

::

  <arg name="camera" default="camera1">
  ......
  <arg name="device_id" default="207000010443">
  ......


``openni2_camera2.launch`` 内容如下：

::

  <arg name="camera" default="camera2">
  ......
  <arg name="device_id" default="207000010113">
  ......


通过运行命令 ``roslaunch openni2_launch openni2_camera1.launch`` 打开序列号为 **207000010443** 的设备。

通过运行命令 ``roslaunch openni2_launch openni2_camera2.launch`` 打开序列号为 **207000010113** 的设备。


.. _multilanguage-label:

上位机环境 (Python、Csharp)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

除了提供支持 C++ 在多平台编译的 Camport SDK，图漾还提供了支持 Python 和 Csharp 编译的 MultiLanguage SDK。

Python (Windows)
+++++++++++++++++++


下载 SDK 
*******************

* 选择以下任意方式，下载 MultiLanguage SDK。

   * 在图漾官网下载中心下载：https://www.percipio.xyz/downloadcenter/。
   
   * 使用 git 指令获取本地副本：打开终端，切换到需存放 SDK 的工作目录下，输入 git clone 命令克隆远程仓库。
  
     ::
      
        git clone https://github.com/alphaliang/pcammls.git

* 选择以下任意方式，下载 Windows Camport3 SDK。

   * 在图漾官网下载中心下载：https://www.percipio.xyz/downloadcenter/。
   
   * 使用 git 指令获取本地副本：打开终端，切换到需存放 SDK 的工作目录下，输入 git clone 命令克隆远程仓库。

     ::

       git clone https://github.com/percipioxyz/camport3.git


安装依赖
*******************

编译前，请先：

* 安装 Python。

* 安装 NumPy 和 OpenCV。

  ::
     
     pip install numpy
     pip install opencv-python -i https://pypi.tuna.tsinghua.edu.cn/simple

* 安装 SWIG（版本要求 4.0.1 及以上）。
  
  下载地址：http://www.swig.org/download.html

* 安装 CMake（版本要求 3.15.0 及以上）。

  下载地址：https://cmake.org/download/

* 安装 Visual Studio。


编译
*******************


按照以下步骤，编译示例代码：

1. 配置环境变量：右键 :guilabel:`此电脑`，选择 :guilabel:`属性` > :guilabel:`高级系统属性` > :guilabel:`环境变量`，在 **环境变量** 页面中将本地 SWIG 的可执行文件路添加到系统变量的 ``Path`` 中。
   
   .. figure:: ../image/setpathforswig.gif
       :width: 600px
       :align: center
       :figclass: align-center


2. 打开 ``pcammls`` 目录下 ``CMakeLists.txt``，修改文件中 camport3 的本地路径、编译目标语言（PYTHON）及 PYTHON 版本 、SWIG 的本地路径，如下图所示： 

   .. figure:: ../image/editcmakelistpython.png
       :width: 400px
       :align: center
       :figclass: align-center

3. 在 ``pcammls`` 目录下创建一个新的文件夹，命名为 ``python_build_x64``。
   
4. 启动 **cmake-gui**，指定源码目录到 ``pcammls``，指定编译输出目录到 ``pcammls/python_build_x64``。
   
   .. figure:: ../image/specifydirectorypython.png
       :width: 500px
       :align: center
       :figclass: align-center

5. 点击 :guilabel:`Configure`，选择 Visual Studio 版本和 X86/X64 版本的设置。

   .. figure:: ../image/clickcongifure_CP.png
       :width: 400px
       :align: center
       :figclass: align-center

6. 点击 :guilabel:`Generate`。

7. 点击 :guilabel:`Open Project` 打开工程后，在 Visual Studio 菜单栏中选择 **Release** 并依次点击 :guilabel:`生成` > :guilabel:`生成解决方法`。

   .. figure:: ../image/pythonmake.png
       :width: 600px
       :align: center
       :figclass: align-center

编译后，将生成的动态库 ``pcammls.py``、 ``_pcammls.pyd`` 以及 camport3 SDK 目录下的 ``tycam.dll`` 复制到 ``pcammls/python`` 目录下，打开 Windows PowerShell 并运行生成的 python 文件即可。
   
   * pcammls.py：python_build_x64/swig/swig/_output
   * pcammls.lib 和 _pcammls.pyd：python_build_x64/swig/Release
   * tycam.dll：camport3/bin/win/hostapp/x64

   .. figure:: ../image/pythonrun.png
       :width: 700px
       :align: center
       :figclass: align-center


Python (Linux)
+++++++++++++++++++

下载 SDK 
*******************

* 选择以下任意方式，下载 MultiLanguage SDK。

   * 在图漾官网下载中心下载：https://www.percipio.xyz/downloadcenter/。
   
   * 使用 git 指令获取本地副本：打开终端，切换到需存放 SDK 的工作目录下，输入 git clone 命令克隆远程仓库。
  
     ::
      
        git clone https://github.com/alphaliang/pcammls.git


* 选择以下任意方式，下载 Windows Camport3 SDK。

   * 在图漾官网下载中心下载：https://www.percipio.xyz/downloadcenter/。
   
   * 使用 git 指令获取本地副本：打开终端，切换到需存放 SDK 的工作目录下，输入 git clone 命令克隆远程仓库。
  
     ::
      
        git clone https://github.com/percipioxyz/camport3.git


安装依赖
*******************

编译前，请先：

1. 安装 python。
2. 打开终端，运行以下命令安装 NumPy 和 OpenCV：

   ::
     
     pip install numpy
     pip install opencv-python -i https://pypi.tuna.tsinghua.edu.cn/simple

3. 下载 SWIG 源码（http://www.swig.org/download.html）至本地，解压后切换至源码目录，依次执行以下命令：
   
   ::
     
     ./configure
     make
     sudo make install


编译
*******************
  
按照以下步骤，编译示例代码：

1. 打开 ``pcammls`` 目录下 ``CMakeLists.txt``，修改 camport3 的本地路径、编译目标语言（PYTHON）及 PYTHON 版本，如下图所示： 

   .. figure:: ../image/editcmakelistpythonlinux.png
       :width: 400px
       :align: center
       :figclass: align-center

2. 在 ``pcammls`` 目录下打开终端并依次执行以下命令：
    
   ::

     mkdir build 
     cd build
     cmake ..
     make
     sudo make install
  
   .. figure:: ../image/pythonlinuxmake.png
       :width: 600px
       :align: center
       :figclass: align-center

   

编译后，根据界面提示切换至目录 ``/usr/local/PYTHON`` 并运行生成的 python 文件即可。

   .. figure:: ../image/linuxrunpython.png
       :width: 600px
       :align: center
       :figclass: align-center
   


Csharp (Windows)
+++++++++++++++++++

下载 SDK 
*******************

* 选择以下任意方式，下载 MultiLanguage SDK。

   * 在图漾官网下载中心下载：https://www.percipio.xyz/downloadcenter/。
   
   * 使用 git 指令获取本地副本：打开终端，切换到需存放 SDK 的工作目录下，输入 git clone 命令克隆远程仓库。
  
     ::
      
        git clone https://github.com/alphaliang/pcammls.git

* 选择以下任意方式，下载 Windows Camport3 SDK。

   * 在图漾官网下载中心下载：https://www.percipio.xyz/downloadcenter/。
   
   * 使用 git 指令获取本地副本：打开终端，切换到需存放 SDK 的工作目录下，输入 git clone 命令克隆远程仓库。
  
     ::
      
        git clone https://github.com/percipioxyz/camport3.git


安装依赖
*******************

编译前，请先：

* 安装 SWIG（版本要求 4.0.1 及以上）。
  
  下载地址：http://www.swig.org/download.html

* 安装 CMake（版本要求 3.15.0 及以上）。

  下载地址：https://cmake.org/download/

* 安装 Visual Studio 及 .NET 组件。


编译
*******************

按照以下步骤，编译示例代码：

1. 配置环境变量：右键 :guilabel:`此电脑`，选择 :guilabel:`属性` > :guilabel:`高级系统属性` > :guilabel:`环境变量`，在 **环境变量** 页面中将本地 SWIG 的可执行文件路添加到系统变量的 ``Path`` 中。
   
   .. figure:: ../image/setpathforswig.gif
       :width: 600px
       :align: center
       :figclass: align-center


2. 打开 ``pcammls`` 目录下 ``CMakeLists.txt``，修改文件中 camport3 的本地路径、编译目标语言（CSHARP）、本地安装的 .NET Framework 版本和 SWIG 的本地路径，如下图所示： 

   .. figure:: ../image/editcmakelistCsharp.png
       :width: 400px
       :align: center
       :figclass: align-center

3. 在 ``pcammls`` 目录下创建一个新的文件夹，命名为 ``csharp_build_x64``。
   
4. 启动 **cmake-gui**，指定源码目录到 ``pcammls``，指定编译输出目录到 ``pcammls/csharp_build_x64``。
   
   .. figure:: ../image/specifydirectoryCsharp.png
       :width: 500px
       :align: center
       :figclass: align-center

5. 点击 :guilabel:`Configure`，选择 Visual Studio 版本和 X86/X64 版本的设置。

   .. figure:: ../image/clickcongifure_CP.png
       :width: 400px
       :align: center
       :figclass: align-center

6. 点击 :guilabel:`Generate`。

7. 点击 :guilabel:`Open Project` 打开工程后，在 Visual Studio 菜单栏中选择 Release 并依次点击 :guilabel:`生成` > :guilabel:`生成解决方法`。

   .. figure:: ../image/Csharpmake.png
       :width: 600px
       :align: center
       :figclass: align-center

编译后，将 camport3 SDK ``camport3/bin/win/hostapp/x64`` 目录下的动态库 ``tycam.dll`` 复制到 ``pcammls/csharp_build_x64/Bin/Release`` 目录下，打开 Windows PowerShell 并运行生成的可执行文件即可。

  
   .. figure:: ../image/csharprun.png
       :width: 700px
       :align: center
       :figclass: align-center





.. _build-camera-environment-label:

上位机与相机交叉编译开发环境
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Percipio 网络深度相机支持用户在相机内部运行自己开发的嵌入式应用，用户可以充分利用相机内部的计算资源进行图像预处理或者应用逻辑处理。支持嵌入式应用的具体相机型号可咨询 Percipio 技术支持。

.. note::
   
   USB 深度相机不支持用户在相机内部运行嵌入式应用。

示例程序编译
+++++++++++++++


1. 下载 Camport SOC SDK，链接：https://github.com/percipioxyz/camport3_soc.git
2. 在 SDK 根目录下执行 ``./build.sh``，执行完成后， ``./build/bin`` 目录下可以看到生成的可执行文件 RawFetchFrame。
3. 通过 scp 或者 sftp 将可执行文件拷贝到相机的用户应用文件夹，请参考  :ref:`应用安装 <binaryinstall-label>`。


用户程序编译
+++++++++++++++

1. 参考 SDK 的 ``./Sample/RawFetchFrame``，在 Sample 目录下创建用户程序目录后开发应用程序。
2. 修改 ``./Sample/CMakeLists.txt``，在 ALL_SAMPLES 中增加用户程序目录路径。
3. 执行 ``./build.sh``，执行完成后， ``./build/bin`` 目录下生成可执行文件。
4. 通过 ``scp`` 或者 ``sftp`` 将可执行文件拷贝到相机的用户应用文件夹，请参考  :ref:`应用安装 <binaryinstall-label>`。

使用 Opencv的程序编译流程
+++++++++++++++++++++++++

1. 参考 SDK 的 ``./Sample/RawFetchFrame``， 在 Sample 目录下创建使用 OpenCV 的程序目录后开发应用程序。
2. 修改 ``./Sample/CMakeLists.txt``，在 ALL_SAMPLES 中增加使用 OpenCV 的程序目录路径。
3. 执行 ``./build.sh opencv``，执行完成后， ``./build/bin`` 目录下生成可执行文件。
4. 通过 ``scp`` 或者 ``sftp`` 将可执行文件拷贝到相机的用户应用文件夹，同时，将需要的 OpenCV 库拷贝到相机，OpenCV 库在 ``./ThirdPartyLib/Opencv-3.4/lib`` 目录下，请参考  :ref:`应用安装 <binaryinstall-label>`。


.. _binaryinstall-label:

程序和库文件的安装
+++++++++++++++++++++

1. 通过 ``scp`` 或者 ``sftp`` 将需要安装的文件拷贝到相机上的 ``/mnt/ram`` 目录下：

   ::

     scp ur_FILES percipio@XXX.XXX.XXX.XXX:/mnt/ram

2. 使用 ``putty`` 或者其他终端远程登录相机：

   ::

      ssh percipio@XXX.XXX.XXX.XXX

3. 在相机内将压缩包解压，并将执行文件以及共享库等拷贝到 ``/usr/local`` 下相应的目录中：

   ::

     cd /mnt/ram
     cp ur_ELFS_FILENNAME /usr/local/bin
     cp ur_lib*.so /usr/local/lib/
     cp ur*config /usr/local/etc

4. 执行 ``sync`` 命令，将修改固化到 flash 中。


.. note::

    1. 拷贝过程分两步，首先务必要复制到 ``/mnt/ram`` 目录下，以防应用程序是压缩打包的文件，解压过程中文件系统空间不够而出错。
    2. 文件少的时候可以不使用用压缩文件，文件较多的时候建议要锁打包之后拷贝到相机上解压缩；目前相机支持 ``tar``、 ``tar.gz``、 ``tar.bz2``、未加密的 ``zip`` 文件这几种压缩文件，分别使用 ``tar -xvf``、 ``tar -xzvf``、 ``tar -xjvf``、 ``unzip`` 命令解压。
    3. 用户二次开发目录 ``/usr/local`` 的最大存储容量为 25MB。
    4. 文件解压缩后，务必复制到用户目录下，否则 ``/mnt/ram`` 中内容重启之后会丢失。
    5. 开发中使用的 ``libtycam.so`` 库文件，相机文件系统中已经包含，用户不必再次拷贝到 ``/usr/local/lib`` 下。
    6. 用户安装的应用程序或者脚本需要添加执行权限 ``chmod a+x ELFS_FILENNAME``。
    7. XXX.XXX.XXX.XXX 为目标相机的 IP 地址。


设置程序开机启动
++++++++++++++++++

1. 登录相机
   ::

     ssh percipio@ XXX.XXX.XXX.XXX

2. 修改启动脚本
   ::

      vi /usr/local/etc/start_app.sh
  
      1 #!/bin/bash
      2 # Created By:      Leon Zhou
      3 # Created Time:    2019-04-26 20:29:12
      4 # Modified Time:   2019-04-26 20:33:22
      5 export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib/
      6 export PATH=$PATH:/usr/local/sbin:/usr/sbin:/sbin
      7
      8 #Add your app start command below

   在第８行添加你的 app 的启动命令，例如：
   ::

       1 #!/bin/bash
       2 # Created By:      Leon Zhou
       3 # Created Time:    2019-04-26 20:29:12
       4 # Modified Time:   2019-04-26 20:33:22
       5 export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib/
       6 export PATH=$PATH:/usr/local/sbin:/usr/sbin:/sbin
       7
       8 #Add your app start command below
       9 /usr/loca/bin/your_app

3. 在控制台执行 ``sync`` 指令写入存储系统，并重启设备。


.. warning::

    1) 调试过程中若需要重启 ``gevcam``，需要运行 ``killall percipio_gev_disd``。
    2) 不要在启动脚本中添加 ``reboot`` 等类似的语句！


网络配置
+++++++++

1. IP 设置

   系统默认使用 DHCP 模式动态获取 IP 地址。

   * 静态 IP 设置

     登录相机执行下方指令或者应用程序或者脚本中修改下方配置文件进行 IP 配置，不建议使用 ifconfig 或者其他方式直接修改 IP，避免相机 IP 管理混乱。此方法修改之后配置文件固化在相机中，在重启相机之后生效。

     ::

         echo "your_ip" > /etc/device_ip
         echo "your_gw" > /etc/device_gw
         echo "your_netmask" > /etc/device_netmask

   * 动态IP设置

    ::

       echo "" > /etc/device_ip
       echo "" > /etc/device_gw
       echo "" > /etc/device_netmask


   .. warning::

      1) 请谨慎修改 IP 地址、子网掩码、网关；如果设置不正确的值，会导致无法连接相机。
      2) 务必在控制台执行 ``sync`` 指令把修改写入存储系统，并重启设备。


2. ntp server 配置

   相机支持 NTP 时间同步，默认使用的 ntp server 为：'cn.ntp.org.cn' '0.ubuntu.pool.ntp.org' '1.ubuntu.pool.ntp.org'。
   
   如果用户已搭建了 ntp server，可以执行下方指令将搭建 ntp server 的 IP 或者域名添加在 ``/etc/ntp_server`` 中：
   ::

      echo your_ntp_server > /etc/ntp_server


