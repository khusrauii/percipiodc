

.. _glossary-label:

术语库
---------

.. _active-stereo-label:

主动双目
~~~~~~~~

Percipio 采用自主知识产权的主动双目技术研发的深度相机包括双目红外图像传感器、彩色图像传感器和激光投射器。

Percipio 主动双目深度相机支持通过 USB 接口或者以太网接口接收上位机指令，实现软件指令触发采集图像；
带硬件触发接口的相机支持外部硬件信号触发，按照触发信号的频率采集图像。

激光投射器发射结构光到被测物体表面，彩色图像传感器采集 RGB 数据，红外图像传感器接收到两路红外数据后，以嵌入式视觉处理器为计算平台，
根据三角测距原理高速计算出视野范围内各点的深度数据，最后将深度数据、红外数据、RGB 数据经接口上传给上位机。

     .. figure:: ../image/system_arc_stereo.png
         :width: 600px
         :align: center 
         :alt: 主动双目相机结构
         :figclass: align-center
     
         主动双目相机结构


.. _tof-label:

飞行时间
~~~~~~~~

Percipio 采用飞行时间（ToF）技术研发的深度相机包括彩色图像传感器、ToF 传感器和激光投射器。

Percipio ToF 相机通过千兆以太网接口接收上位机指令，激光投射器发射调制光到被测物体表面，彩色图像传感器采集 RGB 数据，ToF 传感器接收被测物体表面返回的调制光后，以嵌入式视觉处理器为计算平台，高速计算出视野范围内各点的深度数据，最后将深度数据、红外数据、RGB 数据经接口上传给上位机。

     .. figure:: ../image/system_arc_tof.png
         :width: 450px
         :align: center 
         :alt: ToF相机结构
         :figclass: align-center
 
         ToF 相机结构


.. _depth-image-label:

深度图
~~~~~~


深度图（Depth 图）是深度相机视野内所有点的深度数据构成的一个 16bit 位深的单通道矩阵。为直观地体现不同距离值，在 SDK 示例程序中，输出的深度图均被映射到了 RGB 色彩空间，所以显示出来的结果为 RGB 三通道 8bit 位深的位图。

深度图中的单个像素值是空间中物体的某个点到垂直于左红外镜头光轴并通过镜头光心（深度相机光学零点）平面的垂直距离。深度数据单位为毫米，没有深度数据的点值为 0。深度数据无外参，只提供用于转换点云数据的内参。主动双目相机输出的深度数据无畸变，ToF 相机输出的深度数据有畸变。
     
     .. figure:: ../image/depthdefine.png
         :width: 400px
         :align: center
         :alt: 深度图定义
         :figclass: align-center
     
         深度图定义

**深度图数据格式**
     
     .. figure:: ../image/depthdataformat.png
         :align: center
         :alt: 深度图数据格式
         :figclass: align-center


.. _point3D-image-label:

点云图
~~~~~~


点云图是深度相机视野内所有点的点云信息构成的数据矩阵。每个点的点云信息为三维坐标（x，y，z）。没有三维空间信息的点为（x，y，0）。

**点云图数据格式**
     
     .. figure:: ../image/pcdataformat.png
         :align: center
         :alt: 点云图数据格式
         :figclass: align-center

.. _ir-image-label:

红外图
~~~~~~


红外图（IR 图）是红外图像传感器输出的图像。在输出深度图的时候，部分型号的 Percipio 深度相机输出的红外图是被系统处理过的图像，如果需要看原始的红外图，需要关闭深度图输出。红外图分为左红外图和右红外图，二者均包含内参和畸变参数，但是因为左红外图和深度图是同一个空间坐标系，所以左红外图无外参。

.. _color-image-label:

彩色图
~~~~~~


彩色图（RGB图）是彩色图像传感器输出的图像。彩色图像传感器组件提供了内参，外参，畸变参数。不同型号的 Percipio 深度相机会输出不同数据类型的彩色图。

- 含有硬件 ISP 模块的彩色图像传感器输出为正常的 YUYV422/JPEG 图像，经 OpenCV 处理即可显示为正常色彩空间的彩色图。
- 不含硬件 ISP 模块的彩色图像传感器输出为 RAW BAYER 图像，图像画面可能存在偏色现象，经过 SDK 的软件 ISP 处理（如白平衡），才可以显示为正常色彩空间的彩色图。没有硬件 ISP 模块的彩色图像传感器可以确保输出的图像数据与红外图像数据同步。

