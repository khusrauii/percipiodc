
.. _hardware-connection-label:

硬件连接
------------

Camport SDK 需要配合 Percipio 深度相机使用。Camport SDK 支持所有在售型号的 Percipio 深度相机，关于 Percipio 深度相机的型号和参数，详情请参见 :ref:`产品规格 <camera-param-label>`。

该部分主要介绍相机与计算机的连接方式。

关于相机的供电方式，详情请参见 :ref:`产品规格 <camera-param-label>` 一章中每款相机的详情页。

部分相机支持外接外部触发信号，关于外部触发信号说明，详情请参考 :ref:`外部触发 <external-trigger-label>`。

.. important::

    关于硬件连接前需注意的安全事项，详情请参考 :doc:`安全声明 </COPYRIGHTS>`。


.. _usb-connection-label:

USB 深度相机
~~~~~~~~~~~~

**USB 连接方式1**

通过 USB 线缆直接将相机接入到计算机 USB2.0 接口或者 USB3.0 接口。

.. figure:: ../image/usbcon.png
    :width: 480px
    :align: center
    :alt: usb连接方式1
    :figclass: align-center

    USB 连接方式1

**USB 连接方式2**

通过 USB 线缆直接将相机接入到 USB HUB，USB HUB 接入到计算机 USB2.0 接口或者 USB3.0 接口。当连接多个相机时，为确保相机供电，需要使用能够满足供电要求的有源 HUB。

.. figure:: ../image/usbhub.png
    :width: 480px
    :align: center
    :alt: usb连接方式2
    :figclass: align-center

    USB 连接方式2



.. _net-connection-label:

网络深度相机
~~~~~~~~~~~~~~

Percipio 网络深度相机需要采用外部电源供电，部分型号相机同时支持 POE 供电。网络深度相机默认使用 DHCP 方式从服务器动态获取 IP 地址。

网络深度相机接入到计算机前，确保计算机网卡为自动获取 IP 模式（DHCP）。



**网络连接方式1**

通过千兆以太网线缆直接将相机接入到计算机千兆以太网接口。

相机上电启动后约1分钟，计算机和相机可以成功协商获得 169.254.xx.xx 网段的 IP 地址。

通过 SDK 示例程序 ListDevices 确认相机是否已经获得 IP 地址和设备号后，然后运行 SimpleView_FetchFrame.exe -id <设备号> 查看图像。设备号可从设备标签上获得，也可从枚举结果中获得。

.. figure:: ../image/netcon.png
    :width: 480px
    :align: center
    :alt: 网络连接方式1
    :figclass: align-center

    网络连接方式1


**网络连接方式2**

通过千兆以太网线缆将相机和计算机接入同一台千兆以太网交换机。

相机上电启动后约1分钟，计算机和相机可以成功协商获得 169.254.xx.xx 网段的 IP 地址。

通过 SDK 示例程序 ListDevices 确认相机是否已经获得 IP 地址和设备号后，然后运行 SimpleView_FetchFrame.exe -id <设备号> 查看图像。设备号可从设备标签上获得，也可从枚举结果中获得。

.. figure:: ../image/netswitch.png
    :width: 480px
    :align: center
    :alt: 网络连接方式2
    :figclass: align-center

    网络连接方式2


**网络连接方式3**

通过千兆以太网线缆将相机和计算机接入同一台千兆以太网交换机，该交换机接入支持 DHCP 服务功能的路由器，或者在局域网内开通 DHCP 服务器。

相机上电启动后约1分钟，计算机和相机可以从 DHCP 地址服务器获得 192.168.xx.xx 网段的 IP 地址。

通过 SDK 示例程序 ListDevices 确认相机是否已经获得 IP 地址和设备号后，然后运行 SimpleView_FetchFrame.exe -id <设备号> 查看图像。设备号可从设备标签上获得，也可从枚举结果中获得。

.. figure:: ../image/netroute.png
    :width: 480px
    :align: center
    :alt: 网络连接方式3
    :figclass: align-center

    网络连接方式3


.. note::

   若枚举不到网络深度相机或需修改相机的 IP 地址，请参考 :ref:`应用实例：设置网络深度相机的 IP 地址 <application1-label>`。


