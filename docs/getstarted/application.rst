

.. _application-reference-label:

SDK 应用参考
--------------------

通过运行 SDK 开发包中的 ``lib/win/hostapp/x64`` 中可执行文件，或者运行 ``sample/build/bin/Release`` 目录下的可执行文件，可简单应用相机。

此外，Percipio 提供了在 SDK 基础上二次开发的看图软件 **Percipio Viewer**，支持用户快速浏览深度图、彩色图、红外图和点云图，以及在线调整相机的曝光参数、激光亮度等。 **Percipio Viewer** 安装包下载链接：https://www.percipio.xyz/support。 使用说明请参见 :doc:`Percipio Viewer 用户指南 </viewer>`。

.. _sample-exe-label:

示例程序说明
~~~~~~~~~~~~~

ListDevices

  该示例程序用于枚举连接到上位机上的所有深度相机。
  

DumpAllFeatures

  该示例程序用于枚举深度相机支持的各个组件和属性，以及各个属性支持的读写等信息。
  

ForceDeviceIP
   
  该示例程序用于强制设置网络深度相机的 IP 地址。
  

LoopDetect
   
  该示例程序用于处理因环境不稳定等因素造成的数据连接异常。
  
  
SimpleView_FetchFrame
  
  该示例程序用于深度相机工作在自由采集模式下连续采集图像并输出图像数据。

SimpleView_Callback
   
  该示例程序用于深度相机工作在自由采集模式下连续采集图像，在独立的数据处理线程（防止堵塞图像数据获取）中进行 OpenCV 渲染，并输出图像数据。
  
SimpleView_FetchHisto
  
  该示例程序用于获取图像亮度数据直方图。

SimpleView_MultiDevice
  
  该示例程序用于多台深度相机同时连续采集图像并输出图像数据。
  
SimpleView_Point3D
   
  该示例程序用于获取 3D 点云数据。
  
SimpleView_Registration
  
  该示例程序用于获取深度相机的内参、外参、深度图和彩色图，并将深度图和彩色图对齐。
  
SimpleView_TriggerDelay
 
  该示例程序用于设置硬件触发延时时间，深度相机在接收到硬件触发信号并等待特定延时之后采集图像。

SimpleView_TriggerMode0
 
  该示例程序用于设置深度相机工作在模式 0，相机连续采集图像并以最高帧率输出图像数据。

SimpleView_TriggerMode1
 
  该示例程序用于设置深度相机工作在模式 1，相机收到软触发指令或硬触发信号后采集图像并输出图像数据。

SimpleView_TriggerMode_M2S1
 
  该示例程序用于设置主设备（相机）工作在模式 2，多台从设备（相机）工作在模式 1，以实现多台深度相机级联触发，同时采集图像。
  
  主设备收到上位机发送的软件触发信号后，通过硬件 TRIGGER OUT 接口输出触发信号，同时触发自身采集并输出深度图；从设备收到主设备的硬件触发信号后，采集并输出深度图。


SimpleView_TriggerMode_M3S1
 
  该示例程序用于设置主设备（相机）工作在模式 3，多台从设备（相机）工作在模式 1，以实现多台深度相机按照设置的帧率级联触发，同时采集图像。
  
  主设备按照设置的帧率，通过硬件 TRIGGER OUT 接口输出触发信号，同时触发自身采集并输出深度图；从设备收到主设备的硬件触发信号后，采集并输出深度图。


SimpleView_TriggerMode18
 
  该示例程序用于设置深度相机工作在模式 18，相机每接收到一次软触发指令或硬触发信号后，便按照设置的帧率，以 1+duty 的方式采集一轮图像并输出图像数据（1：出 1 次泛光；duty：出 duty 次激光）。

SimpleView_TriggerMode19
 
  该示例程序用于设置深度相机工作在模式 19，相机接收到一次软触发或者硬触发信号后，便按照设置的帧率，以 1+duty 的方式连续采集图像并输出图像数据（1：出 1 次泛光；duty：出 duty 次激光）。

SimpleView_TriggerMode20
 
  该示例程序用于设置深度相机工作在模式 20，根据设置的触发开始时间（start_time_us）、每两帧的时间间隔数组（offset_us_list[ ]）和触发次数（offset_us_count），相机定时采集（1 + offset_us_count）帧图像并输出图像数据。启用此⼯作模式要求相机先启动 PTP 对时，且 offset_us_count ≤ 50。

SimpleView_TriggerMode21
 
  该示例程序用于设置深度相机工作在模式 21，根据设置的触发开始时间（start_time_us）、触发次数（trigger_count）和触发时间间隔（peroid_us），相机每间隔 peroid_us 采集⼀帧图像，共采集 trigger_count 帧图像并输出图像数据。此工作模式要求相机先启动 PTP 对时。

.. _application1-label:

应用实例：设置网络深度相机的 IP 地址
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

设置网络深度相机 IP 地址的示例程序：ForceDeviceIP

**说明**

* 设置为临时 IP 地址

  指令：ForceDeviceIP.exe -force <MAC> <newIP> <newNetmask> <newGateway>

  其中，<MAC> 可从设备标签上获得，格式一般为：xx:xx:xx:xx:xx:xx；<newIP> 为指定 IP 地址；<newNetmask> 和 <newGateway> 根据 newIP 设置。该指令执行后，网络深度相机 IP 地址会修改为指令指定的 IP 地址，即时生效；相机断电重启后，恢复原有配置。

  示例：ForceDeviceIP.exe -force 68:f7:56:36:90:a3 192.168.1.160 255.255.255.0 192.168.1.1


* 设置为静态 IP 地址

  指令：ForceDeviceIP.exe -static <MAC> <newIP> <newNetmask> <newGateway>

  其中，<MAC> 可从设备标签上获得，格式一般为：xx:xx:xx:xx:xx:xx；<newIP> 为指定 IP 地址；<newNetmask> 和 <newGateway> 根据 newIP 设置。该指令执行后，网络深度相机 IP 地址会修改为指令指定的 IP 地址，即时生效；相机断电重启后，IP 地址保持为指令配置的地址不变。

  示例：ForceDeviceIP.exe -static 68:f7:56:36:90:a3 192.168.1.160 255.255.255.0 192.168.1.1


* 设置为动态 IP 地址（新版相机支持）
 
  指令：ForceDeviceIP.exe -dynamic <MAC>

  其中，<MAC> 可从设备标签上获得，格式一般为：xx:xx:xx:xx:xx:xx。该指令执行后，会清空网络深度相机 IP 配置，恢复为 DHCP 动态获取方式并立即动态申请 IP 地址；相机断电重启后，设备主动以 DHCP 方式申请 IP。某些旧版本相机不支持该指令。

  示例：ForceDeviceIP.exe -dynamic 68:f7:56:36:90:a3


* 设置为动态 IP 地址（旧/新版相机支持）

  指令：ForceDeviceIP.exe -dynamic <MAC> <newIP> <newNetmask> <newGateway>

  其中，<MAC> 可从设备标签上获得，格式一般为：xx:xx:xx:xx:xx:xx；<newIP> <newNetmask> <newGateway> 为临时使用的 IP 地址、掩码。该指令执行后，会临时使用 newIP 配置相机，然后清空相机 IP 配置，恢复为 DHCP 动态获取方式并立即动态申请 IP 地址。相机断电重启后，设备保持以 DHCP 方式申请 IP 地址。

  示例：ForceDeviceIP.exe -dynamic 68:f7:56:36:90:a3 192.168.1.160 255.255.255.0 192.168.1.1


**应用场景1**

通过 SDK 示例程序 ListDevices 无法枚举到 Percipio 网络深度相机。

以 Windows 10 为例，操作步骤如下：

1. 在 SDK sample/build/bin/Release 目录下执行指令 ForceDeviceIP.exe -force <MAC> <newIP> <Netmask> <Gateway>，新设的 newIP 需与计算机的 IP 地址处于同一网段，子网掩码和网关和计算机一致。

2. 按需执行以下指令，将相机的 IP 地址设为：

   - 动态 IP 地址：ForceDeviceIP.exe -dynamic <MAC> 或 ForceDeviceIP.exe -dynamic <MAC> <newIP> <newNetmask> <newGateway>
   
   - 静态 IP 地址：ForceDeviceIP.exe -static <MAC> <newIP> <newNetmask> <newGateway>



**应用场景2**

将 Percipio 网络深度相机的动态 IP 地址修改成静态 IP 地址。

以 Windows 10 为例，操作步骤如下：

1. 若新修改的静态 IP 地址与计算机的 IP 地址不在同一网段，先修改计算机的 IP 地址。

   例如：若新修改的静态 IP 地址为 192.168.5.XX，打开电脑上的控制面板，选择 “网络和 Internet” > “网络和共享中心” > “更改适配器配置” > “以太网” > “Internet 协议板块4（TCP/IPv4）”，在弹出的 **Internet 协议板块4（TCP/IPv4）属性** 对话框中选中 “使用下面的 IP 地址” 并设置 IP 地址、子网掩码和网关：

   .. figure:: ../image/modify-pc-ip.png
      :width: 500px
      :align: center
      :alt: 修改计算机 IP 地址
      :figclass: align-center

      修改计算机 IP 地址

2. 进入 SDK sample/build/bin/Release 目录下执行命令：ForceDeviceIP.exe -static <MAC> <newIP> <Netmask> <Gateway>，newIP 是需要修改成的静态 IP 地址。

