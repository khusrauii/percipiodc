Resources
===========


* Join the QQ group (602754096) to ask questions and find community resources.

* Check the `Issues <https://github.com/percipioxyz/camport3/issues>`_  section on GitHub if you find a bug or have a feature request. Please check existing `Issues <https://github.com/percipioxyz/camport3/issues>`_ before opening a new one.

* To develop applications using OpenNI2 platform, refer to `OpenNI2 <https://github.com/percipioxyz/camport3_openni2>`_.

* To develop applications using ROS platform, refer to `ROS <https://github.com/percipioxyz/camport_ros>`_.

* For additional Percipio Depth Camera product related information, please refer to `official website <https://www.percipio.xyz/>`_ site or send email to support@percipio.xyz.

