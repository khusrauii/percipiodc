API Reference
===============


Introduction
--------------

The camport3 libraries provide support for configuring PERCIPIO depth camera, data receiving and post-processing functionality.

API for Percipio depth camera
-------------------------------

.. doxygenfile:: TYApi.h


API for coodinate mapping functions
-------------------------------------

.. doxygenfile:: TYCoordinateMapper.h


API for Depth and IR image post processing
-------------------------------------------

.. doxygenfile:: TYImageProc.h


API for Color image post processing
-------------------------------------

.. doxygenfile:: TyIsp.h



