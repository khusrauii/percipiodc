基础概念
=============

组件
++++++

每个 Percipio 深度相机包含多个器件，如包括左红外图像传感器、右红外图像传感器、激光投射器、彩色图像传感器、IMU 等器件，我们把这些器件统称为组件（Component）。

组件也可能是虚拟器件，例如深度图像传感器、直方图设备等，尽管并不存在这些实体器件，从数据访问的角度并没有什么区别，用户可以同操作真实存在的器件一样操作这些虚拟器件，完成相应的功能设置。

Percipio 深度相机作为一个设备，拥有工作模式相关配置、连接状态保持、网络配置与数据重传配置、光学外参等属性，为实现对这些属性的访问，我们把相机做为一个抽象组件，即设备 (Device) 组件。

相机包含的全体组件定义如下：

::

    typedef enum TY_DEVICE_COMPONENT_LIST
    {
        TY_COMPONENT_DEVICE         = 0x80000000, ///< Abstract component stands for whole device, always enabled
        TY_COMPONENT_DEPTH_CAM      = 0x00010000, ///< Depth camera
        TY_COMPONENT_IR_CAM_LEFT    = 0x00040000, ///< Left IR camera
        TY_COMPONENT_IR_CAM_RIGHT   = 0x00080000, ///< Right IR camera
        TY_COMPONENT_RGB_CAM_LEFT   = 0x00100000, ///< Left RGB camera
        TY_COMPONENT_RGB_CAM_RIGHT  = 0x00200000, ///< Right RGB camera
        TY_COMPONENT_LASER          = 0x00400000, ///< Laser
        TY_COMPONENT_IMU            = 0x00800000, ///< Inertial Measurement Unit
        TY_COMPONENT_BRIGHT_HISTO   = 0x01000000, ///< virtual component for brightness histogram of ir
        TY_COMPONENT_STORAGE        = 0x02000000, ///< virtual component for device storage
    
        TY_COMPONENT_RGB_CAM        = TY_COMPONENT_RGB_CAM_LEFT ///< Some device has only one RGB camera, map it to left
    }TY_DEVICE_COMPONENT_LIST;


其中，

* TY_COMPONENT_DEVICE 代表独立深度相机实体，以下简称 **深度相机**。通过该组件可以控制相机的工作模式、内部 Sensor 曝光同步模式、光学外参等属性。
* TY_COMPONENT_DEPTH_CAM 代表深度图像传感器组件。
* TY_COMPONENT_IR_CAM_LEFT 代表左红外图像传感器组件。
* TY_COMPONENT_IR_CAM_RIGHT 代表右红外图像传感器组件。
* TY_COMPONENT_RGB_CAM_LEFT 代表左彩色图像传感器组件。若深度相机仅有一个彩色图像传感器组件，TY_COMPONENT_RGB_CAM 默认代表 TY_COMPONENT_RGB_CAM_LEFT。
* TY_COMPONENT_RGB_CAM_RIGHT 代表右彩色图像传感器组件。



属性
+++++

深度相机或者各个组件有多种不同的功能参数，SDK 软件工程中称之为属性 (feature)。依据各个属性的特点，SDK 分别为这些属性定义 int、bool、struct、enum 等类型的数据结构。用户可以通过 TYGetInt / TYSetInt / TYGetStruct / TYSetStruct 等类似风格的 API 函数，读取或者写入某个组件的某个属性。

不同组件的属性之间也可以存在联动关系。修改组件的一个属性时，深度相机会同步修改另一个组件的同一属性，这种关系称为绑定 (binding)。绑定关系可以通过查询属性信息得知。

**不同型号、不同版本的深度相机支持不同的组件和属性。**

.. note::

   通过以下方式，可查询深度相机支持的组件和和属性：

   #. 通过 API 可查询深度相机支持的组件和属性。
   #. 通过 SDK 示例程序 DumpAllFeatures 可列出当前操作相机的所有组件和支持的属性信息。
   #. 通过 SDK 示例程序 DumpAllFeatures -d，可以生成 fetch_config.xml 文件，在该文件中可以查看深度相机支持的组件和和属性。

以 FM851-E1 为例，该相机包含的组件、支持的属性以及属性与组件的隶属关系，请参考下方 fetch_config.xml 文件内的配置描述：
::

    <?xml version="1.0" ?>
    <config version="1">
    	<comprotocol>1.0</comprotocol>
    	<component addr="0x01000000" id="0x80000000" name="device">
    		<feature hide="0" id="0x5f00" name="SN" rw="1">207000129217</feature>
    		<feature hide="0" id="0x5f01" name="vendor" rw="1">Percipio</feature>
    		<feature hide="0" id="0x5f02" name="model" rw="1">FM851-E1</feature>
    		<feature hide="0" id="0x5f08" name="user defined name" rw="1">Percipio</feature>
    		<feature addr="0x204" hide="0" id="0x1202" name="frame per trigger" rw="3">1</feature>
    		<feature addr="0x208" hide="0" id="0x7523" name="struct trigger mode" rw="3"/>
    		<feature addr="0x218" hide="0" id="0x7525" name="struct trigger mode ex" rw="3"/>
    		<feature absaddr="0x64c" hide="0" id="0x1010" name="persistent IP" rw="3"/>
    		<feature absaddr="0x65c" hide="0" id="0x1011" name="persistent netmask" rw="3"/>
    		<feature absaddr="0x66c" hide="0" id="0x1012" name="persistent gateway" rw="3"/>
    		<feature addr="0x400020" hide="0" id="0x1014" inc="1" max="10000" min="0" name="packet delay" rw="3"/>
    		<feature addr="0x40002c" hide="0" id="0x1017" inc="1" max="1500" min="100" name="packet size" rw="3"/>
    		<feature addr="0x400024" hide="0" id="0x1016" name="ntp server ip" rw="3"/>
    		<feature addr="0x214" hide="0" id="0x4207" name="trigger out io" rw="2" writableAtRun="1"/>
    		<feature addr="0x20c" hide="0" id="0x4205" name="cmos sync" rw="3"/>
    		<feature hide="0" id="0x4203" name="keep alive onoff" rw="3"/>
    		<feature hide="0" id="0x1204" inc="1" max="30000" min="2000" name="keep alive timeout" rw="3"/>
    		<feature addr="0x210" hide="0" id="0x1206" inc="1" max="1300000" min="0" name="trigger delay us" rw="3"/>
    		<feature addr="0x21C" hide="0" id="0x1208" inc="1" max="1300000" min="1" name="trigger duration us" rw="3"/>
    		<feature addr="0x220" hide="0" id="0x3209" name="stream async" rw="3">
    			<entry name="async off" value="0"/>
    			<entry name="async depth" value="1"/>
    			<entry name="async rgb" value="2"/>
    			<entry name="async depth rgb" value="3"/>
    			<entry name="async all" value="255"/>
    		</feature>
    		<feature addr="0x200" hide="0" id="0x3201" name="trig pol" rw="3">
    			<entry name="falling edge" value="0"/>
    			<entry name="rising edge" value="1"/>
    		</feature>
    		<feature addr="0x228" hide="0" id="0x1210" name="capture time us" rw="1" volatile="1"/>
    		<feature addr="0x22c" hide="0" id="0x3211" name="sync type" rw="3" volatile="1">
    			<entry name="sync type none" value="0"/>
    			<entry name="sync type host" value="1"/>
    			<entry name="sync type ntp" value="2"/>
    			<entry name="sync type ptp slave" value="3"/>
    			<entry name="sync type can" value="4"/>
    			<entry name="sync type ptp master" value="5"/>
    		</feature>
    		<feature addr="0x230" hide="0" id="0x4212" name="sync ready" rw="1" volatile="1"/>
    		<feature addr="0x234" hide="0" id="0x7526" name="trigger timer list" rw="3" volatile="1" writableAtRun="1"/>
    		<feature addr="0x238" hide="0" id="0x7527" name="trigger timer period" rw="3" volatile="1" writableAtRun="1"/>
    		<feature id="0x5f03" name="hardware version" rw="1" hide="0">1.3.0</feature>
    		<feature id="0x5f04" name="firmware version" rw="1" hide="0">3.13.16</feature>
    		<build>
    			<hash>model:AP03B04AM81X_X_X7_F121_DVP;rtl:R1.1.13_0_gcf0ff384e3;kernel:R3.13.16-0-g98f7c91b8d;tycam:R3.5.18-6b7b483c;gevcam:R3.13.16-0-g9ac78d34</hash>
    		</build>
    		<config_version>pre_release</config_version>
    		<tech_model>AM830-GTIS8-47-CBO</tech_model>
    		<generated_time>2021-10-09T135051</generated_time>
    		<calibration_time>2021-09-18T033241</calibration_time>
    	</component>
    	<component addr="0x02000000" id="0x00010000" name="depth">
    		<feature hide="0" id="0x210a" name="scale unit" rw="1">1.0</feature>
    		<feature hide="1" id="0x1003" name="intrinsic width" rw="1">1280</feature>
    		<feature hide="1" id="0x1004" name="intrinsic height" rw="1">960</feature>
    		<feature hide="0" id="0x7000" name="depth intrinsic" rw="1"> 1057.0913560361116 0.0 628.5735321044922 0.0 1057.0913560361116 490.0430450439453 0.0 0.0 1.0</feature>
    		<feature addr="0x10" hide="0" id="0x1104" name="image width" rw="1"/>
    		<feature addr="0x14" hide="0" id="0x1105" name="image height" rw="1"/>
    		<feature addr="0x18" hide="0" id="0x3109" name="image mode" rw="3">
    			<entry name="DEPTH16_640x480" value="0x202801e0"/>
    			<entry name="DEPTH16_1280x960" value="0x205003c0"/>
    			<entry name="DEPTH16_320x240" value="0x201400f0"/>
    		</feature>
    	</component>
    	<component addr="0x03000000" id="0x00040000" name="leftir">
    		<feature addr="0x18" hide="0" id="0x3109" name="image mode" rw="1">
    			<entry name="mono8 1280x960" value="0x105003c0"/>
    		</feature>
    		<feature addr="0x10" hide="0" id="0x1104" name="image width" rw="1">1280</feature>
    		<feature addr="0x14" hide="0" id="0x1105" name="image height" rw="1">960</feature>
    		<feature hide="1" id="0x1003" name="intrinsic width" rw="1">1280</feature>
    		<feature hide="1" id="0x1004" name="intrinsic height" rw="1">960</feature>
    		<feature hide="0" id="0x7000" name="intrinsic" rw="1"> 1101.9569754361974 0.0 632.6048087423237 0.0 1102.146112084317 476.4526537628572 0.0 0.0 1.0</feature>
    		<feature hide="0" id="0x7006" name="distortion" rw="1"> -0.4205079032648662 -0.24291850706937249 0.0032100257861444777 0.001990176887732496 0.10267820146085133 -0.1716891282414848 -0.44131454807106735 0.11949232513267484 -0.003917085741072632 0.0008741779334892755 -0.0070731359600290965 0.001578093155976139</feature>
    		<feature addr="0x324" hide="0" id="0x4510" name="undistort" rw="3"/>
    		<feature addr="0x304" hide="0" id="0x1301" inc="1" max="1088" min="3" name="exposure time" rw="3" writableAtRun="1"/>
    		<feature addr="0x310" hide="0" id="0x1303" inc="1" max="255" min="0" name="gain" rw="3" writableAtRun="1"/>
    		<feature addr="0x328" hide="0" id="0x1524" inc="1" max="3" min="0" name="analog gain" rw="3" writableAtRun="1"/>
    	</component>
    	<component addr="0x04000000" id="0x00080000" name="rightir">
    		<feature addr="0x18" hide="0" id="0x3109" name="image mode" rw="1">
    			<entry name="mono8 1280x960" value="0x105003c0"/>
    		</feature>
    		<feature addr="0x10" hide="0" id="0x1104" name="image width" rw="1">1280</feature>
    		<feature addr="0x14" hide="0" id="0x1105" name="image height" rw="1">960</feature>
    		<feature hide="1" id="0x1003" name="intrinsic width" rw="1">1280</feature>
    		<feature hide="1" id="0x1004" name="intrinsic height" rw="1">960</feature>
    		<feature hide="0" id="0x7000" name="intrinsic" rw="1"> 1103.249499342999 0.0 630.7622752161499 0.0 1103.2889923805135 509.1805648068269 0.0 0.0 1.0</feature>
    		<feature hide="0" id="0x7006" name="distortion" rw="1"> 0.1607226089625935 0.04310528484860882 0.004990395649432428 0.0008160696949675246 0.4613061089900877 0.4151237879065313 -0.030265818405065734 0.5510116415135468 -0.0011233127606329815 0.00022496932265199835 -0.010514656609723044 0.0024125513606033243</feature>
    		<feature hide="0" id="0x7001" name="rightIR to leftIR extrinsic" rw="1"> 0.9999855582465992 -0.0024780726921431236 0.004768904902616075 -78.91377474894921 0.002521488192062276 0.9999552374414395 -0.009119496188449452 0.24564214899255385 -0.0047460926597604315 0.009131389224335365 0.9999470447655208 -0.12529605489830792 0 0 0 1</feature>
    		<feature bind="0x00044510"/>
    		<feature addr="0x304" hide="0" id="0x1301" inc="1" max="1088" min="3" name="exposure time" rw="3" writableAtRun="1"/>
    		<feature addr="0x310" hide="0" id="0x1303" inc="1" max="255" min="0" name="gain" rw="3" writableAtRun="1"/>
    		<feature addr="0x328" hide="0" id="0x1524" inc="1" max="3" min="0" name="analog gain" rw="3" writableAtRun="1"/>
    	</component>
    	<component addr="0x05000000" id="0x00100000" name="rgb">
    		<feature addr="0x10" hide="0" id="0x1104" name="image width" rw="1" volatile="1"/>
    		<feature addr="0x14" hide="0" id="0x1105" name="image height" rw="1" volatile="1"/>
    		<feature addr="0x18" hide="0" id="0x3109" name="image mode" rw="3">
    			<entry name="yuyv 1280x960" value="0x225003c0"/>
    			<entry name="yuyv 640x480" value="0x222801e0"/>
    			<entry name="yuyv 320x240" value="0x221400f0"/>
    		</feature>
    		<feature addr="0x224" hide="0" id="0x7305" name="struct aec roi" rw="3" writableAtRun="1"/>
    		<feature addr="0x300" hide="0" id="0x4300" name="auto exp ctrl" rw="3" writableAtRun="1"/>
    		<feature addr="0x304" hide="0" id="0x1301" inc="1" max="1088" min="3" name="exposure time" rw="3" writableAtRun="1"/>
    		<feature addr="0x314" hide="0" id="0x4304" name="auto white balance" rw="3" writableAtRun="1"/>
    		<feature addr="0x318" hide="0" id="0x1520" inc="1" max="4095" min="1" name="r gain" rw="3" writableAtRun="1"/>
    		<feature addr="0x31c" hide="0" id="0x1521" inc="1" max="4095" min="1" name="g gain" rw="3" writableAtRun="1"/>
    		<feature addr="0x320" hide="0" id="0x1522" inc="1" max="4095" min="1" name="b gain" rw="3" writableAtRun="1"/>
    		<feature addr="0x328" hide="0" id="0x1524" inc="1" max="63" min="1" name="analog gain" rw="3" writableAtRun="1"/>
    		<feature addr="0x30c" hide="0" id="0x4302" name="auto gain ctrl" rw="3" writableAtRun="1"/>
    		<feature hide="1" id="0x1003" name="intrinsic width" rw="1">1280</feature>
    		<feature hide="1" id="0x1004" name="intrinsic height" rw="1">960</feature>
    		<feature hide="0" id="0x7000" name="rgb intrinsic" rw="1"> 1091.57297791 0.0 639.331289979 0.0 1091.44752035 503.323233757 0.0 0.0 1.0</feature>
    		<feature hide="0" id="0x7001" name="rgb to leftIR extrinsic" rw="1"> 0.999986864606 -0.00117676964364 0.00498856976817 24.0101958417 0.00117657829273 0.999999306979 4.12924369618e-05 -0.20405264306 -0.00498861490267 -3.54224516683e-05 0.999987556156 -0.534259434997 0.0 0.0 0.0 1.0</feature>
    		<feature hide="0" id="0x7006" name="distortion" rw="1">0.168248655694 0.0613630693992 0.00541864468694 0.0015374141221 0.514789735342 0.414091440597 0.000124699681652 0.590942851883 -0.00271625606685 0.000477951236185 -0.010361247278 0.00262128322074</feature>
    	</component>
    	<component addr="0x07000000" id="0x00400000" name="laser">
    		<feature addr="0x500" hide="0" id="0x1500" inc="1" max="100" min="0" name="power" rw="3" writableAtRun="1"/>
    		<feature addr="0x504" hide="0" id="0x4501" name="auto ctrl" rw="3"/>
    	</component>
    	<component addr="0x08000000" id="0x01000000" name="histogram">    </component>
    	<component addr="0x09000000" id="0x02000000" name="storage">
    		<feature addr="0x100000" hide="0" id="0x600a" name="custom block" rw="3" size="4096" volatile="1"/>
    		<feature addr="0x200000" hide="0" id="0x600b" name="isp block" rw="3" size="65536" volatile="1"/>
    	</component>
    </config>


根据上述的 fetch_config.xml 文件，可获得该相机包括的组件及各个组件支持的部分属性：


.. list-table:: 
   :header-rows: 1

   * - 组件
     - 部分属性
   * - device
     - SN、model、packet delay、packet size、trigger out io、cmos sync、keep alive onoff、stream async、trig pol、capture time us、sync type
   * - depth
     - image width、image height、image mode
   * - leftir / rightir
     - image mode、image width、image height、undistort、exposure time、gain、analog gain 
   * - rgb
     - image width、image height、image mode、exposure time、auto white balance、r gain、g gain、b gain、analog gain、auto gain ctrl
   * - laser
     - power、 auto ctrl
   * - histogram
     - 无属性，该组件专门提供 histogram 数据。
   * - storage
     - custom block、isp block

.. note::

     1. <component></component>一对组件标签内介绍了该组件支持的属性。
     2. <feature></feature>一对属性标签内介绍了该属性的名称、最大值、最小值、读写权限、设置方式，部分属性枚举了可设置值。
   
        - 读写权限：rw=1 表示只读，rw=3 表示可读写。
        - 设置方式：writableAtRun=1 表示该属性可在相机采图过程中设置，没有此说明的属性则需在相机开始采图前设置。


不同的属性的数据值分为整型（INT）、布尔型（BOOL）、浮点型（FLOAT）、枚举型（ENUM）和结构体型（STRUCT），具体内容请参考属性定义：

::

    typedef enum TY_FEATURE_ID_LIST
    {
        TY_STRUCT_CAM_INTRINSIC         = 0x0000 | TY_FEATURE_STRUCT, ///< see TY_CAMERA_INTRINSIC
        TY_STRUCT_EXTRINSIC_TO_DEPTH    = 0x0001 | TY_FEATURE_STRUCT, ///< extrinsic between  depth cam and current component , see TY_CAMERA_EXTRINSIC
        TY_STRUCT_EXTRINSIC_TO_IR_LEFT  = 0x0002 | TY_FEATURE_STRUCT, ///< extrinsic between  left IR and current compoent, see TY_CAMERA_EXTRINSIC
        TY_STRUCT_CAM_DISTORTION        = 0x0006 | TY_FEATURE_STRUCT, ///< see TY_CAMERA_DISTORTION
        TY_STRUCT_CAM_CALIB_DATA        = 0x0007 | TY_FEATURE_STRUCT, ///< see TY_CAMERA_CALIB_INFO
        TY_BYTEARRAY_CUSTOM_BLOCK       = 0x000A | TY_FEATURE_BYTEARRAY, ///< used for reading/writing custom block
        TY_BYTEARRAY_ISP_BLOCK          = 0x000B | TY_FEATURE_BYTEARRAY, ///< used for reading/writing fpn block
    
        TY_INT_PERSISTENT_IP            = 0x0010 | TY_FEATURE_INT,
        TY_INT_PERSISTENT_SUBMASK       = 0x0011 | TY_FEATURE_INT,
        TY_INT_PERSISTENT_GATEWAY       = 0x0012 | TY_FEATURE_INT,
        TY_BOOL_GVSP_RESEND             = 0x0013 | TY_FEATURE_BOOL,
        TY_INT_PACKET_DELAY             = 0x0014 | TY_FEATURE_INT,    ///< microseconds
        TY_INT_ACCEPTABLE_PERCENT       = 0x0015 | TY_FEATURE_INT,
        TY_INT_NTP_SERVER_IP            = 0x0016 | TY_FEATURE_INT,    ///< Ntp server IP
        TY_INT_PACKET_SIZE              = 0x0017 | TY_FEATURE_INT,
        TY_STRUCT_CAM_STATISTICS        = 0x00ff | TY_FEATURE_STRUCT, ///< statistical information, see TY_CAMERA_STATISTICS
    
        TY_INT_WIDTH_MAX                = 0x0100 | TY_FEATURE_INT,
        TY_INT_HEIGHT_MAX               = 0x0101 | TY_FEATURE_INT,
        TY_INT_OFFSET_X                 = 0x0102 | TY_FEATURE_INT,
        TY_INT_OFFSET_Y                 = 0x0103 | TY_FEATURE_INT,
        TY_INT_WIDTH                    = 0x0104 | TY_FEATURE_INT,  ///< Image width
        TY_INT_HEIGHT                   = 0x0105 | TY_FEATURE_INT,  ///< Image height
        TY_ENUM_IMAGE_MODE              = 0x0109 | TY_FEATURE_ENUM, ///< Resolution-PixelFromat mode, see TY_IMAGE_MODE_LIST
    
        //@brief scale unit
        //depth image is uint16 pixel format with default millimeter unit ,for some device  can output Sub-millimeter accuracy data
        //the acutal depth (mm)= PixelValue * ScaleUnit 
        TY_FLOAT_SCALE_UNIT             = 0x010a | TY_FEATURE_FLOAT, 
    
        TY_ENUM_TRIGGER_POL             = 0x0201 | TY_FEATURE_ENUM,  ///< Trigger POL, see TY_TRIGGER_POL_LIST
        TY_INT_FRAME_PER_TRIGGER        = 0x0202 | TY_FEATURE_INT,  ///< Number of frames captured per trigger
        TY_STRUCT_TRIGGER_PARAM         = 0x0523 | TY_FEATURE_STRUCT,  ///< param of trigger, see TY_TRIGGER_PARAM
        TY_STRUCT_TRIGGER_PARAM_EX      = 0x0525 | TY_FEATURE_STRUCT,  ///< param of trigger, see TY_TRIGGER_PARAM_EX
        TY_STRUCT_TRIGGER_TIMER_LIST    = 0x0526 | TY_FEATURE_STRUCT,  ///< param of trigger mode 20, see TY_TRIGGER_TIMER_LIST
        TY_STRUCT_TRIGGER_TIMER_PERIOD  = 0x0527 | TY_FEATURE_STRUCT,  ///< param of trigger mode 21, see TY_TRIGGER_TIMER_PERIOD
        TY_BOOL_KEEP_ALIVE_ONOFF        = 0x0203 | TY_FEATURE_BOOL, ///< Keep Alive switch
        TY_INT_KEEP_ALIVE_TIMEOUT       = 0x0204 | TY_FEATURE_INT,  ///< Keep Alive timeout
        TY_BOOL_CMOS_SYNC               = 0x0205 | TY_FEATURE_BOOL, ///< Cmos sync switch
        TY_INT_TRIGGER_DELAY_US         = 0x0206 | TY_FEATURE_INT,  ///< Trigger delay time, in microseconds
        TY_BOOL_TRIGGER_OUT_IO          = 0x0207 | TY_FEATURE_BOOL, ///< Trigger out IO
        TY_INT_TRIGGER_DURATION_US      = 0x0208 | TY_FEATURE_INT,  ///< Trigger duration time, in microseconds
        TY_ENUM_STREAM_ASYNC            = 0x0209 | TY_FEATURE_ENUM,  ///< stream async switch, see TY_STREAM_ASYNC_MODE
        TY_INT_CAPTURE_TIME_US          = 0x0210 | TY_FEATURE_INT,  ///< capture time in multi-ir 
        TY_ENUM_TIME_SYNC_TYPE          = 0x0211 | TY_FEATURE_ENUM, ///< see TY_TIME_SYNC_TYPE
        TY_BOOL_TIME_SYNC_READY         = 0x0212 | TY_FEATURE_BOOL,
        TY_BOOL_FLASHLIGHT              = 0x0213 | TY_FEATURE_BOOL,
        TY_INT_FLASHLIGHT_INTENSITY     = 0x0214 | TY_FEATURE_INT,
    
        TY_BOOL_AUTO_EXPOSURE           = 0x0300 | TY_FEATURE_BOOL, ///< Auto exposure switch
        TY_INT_EXPOSURE_TIME            = 0x0301 | TY_FEATURE_INT,  ///< Exposure time in percentage
        TY_BOOL_AUTO_GAIN               = 0x0302 | TY_FEATURE_BOOL, ///< Auto gain switch
        TY_INT_GAIN                     = 0x0303 | TY_FEATURE_INT,  ///< Sensor Gain
        TY_BOOL_AUTO_AWB                = 0x0304 | TY_FEATURE_BOOL, ///< Auto white balance
        TY_STRUCT_AEC_ROI               = 0x0305 | TY_FEATURE_STRUCT,  ///< region of aec statistics, see TY_AEC_ROI_PARAM
    
        TY_INT_LASER_POWER              = 0x0500 | TY_FEATURE_INT,  ///< Laser power level
        TY_BOOL_LASER_AUTO_CTRL         = 0x0501 | TY_FEATURE_BOOL, ///< Laser auto ctrl
    
        TY_BOOL_UNDISTORTION            = 0x0510 | TY_FEATURE_BOOL, ///< Output undistorted image
        TY_BOOL_BRIGHTNESS_HISTOGRAM    = 0x0511 | TY_FEATURE_BOOL, ///< Output bright histogram
        TY_BOOL_DEPTH_POSTPROC          = 0x0512 | TY_FEATURE_BOOL, ///< Do depth image postproc
    
        TY_INT_R_GAIN                   = 0x0520 | TY_FEATURE_INT,  ///< Gain of R channel
        TY_INT_G_GAIN                   = 0x0521 | TY_FEATURE_INT,  ///< Gain of G channel
        TY_INT_B_GAIN                   = 0x0522 | TY_FEATURE_INT,  ///< Gain of B channel
    
        TY_INT_ANALOG_GAIN              = 0x0524 | TY_FEATURE_INT,  ///< Analog gain
        TY_BOOL_HDR                     = 0x0525 | TY_FEATURE_BOOL,
        TY_BYTEARRAY_HDR_PARAMETER      = 0x0526 | TY_FEATURE_BYTEARRAY,
    
        TY_BOOL_IMU_DATA_ONOFF          = 0x0600 | TY_FEATURE_BOOL, ///< IMU Data Onoff
        TY_STRUCT_IMU_ACC_BIAS          = 0x0601 | TY_FEATURE_STRUCT, ///< IMU acc bias matrix, see TY_ACC_BIAS
        TY_STRUCT_IMU_ACC_MISALIGNMENT  = 0x0602 | TY_FEATURE_STRUCT, ///< IMU acc misalignment matrix, see TY_ACC_MISALIGNMENT
        TY_STRUCT_IMU_ACC_SCALE         = 0x0603 | TY_FEATURE_STRUCT, ///< IMU acc scale matrix, see TY_ACC_SCALE
        TY_STRUCT_IMU_GYRO_BIAS         = 0x0604 | TY_FEATURE_STRUCT, ///< IMU gyro bias matrix, see TY_GYRO_BIAS
        TY_STRUCT_IMU_GYRO_MISALIGNMENT = 0x0605 | TY_FEATURE_STRUCT, ///< IMU gyro misalignment matrix, see TY_GYRO_MISALIGNMENT
        TY_STRUCT_IMU_GYRO_SCALE        = 0x0606 | TY_FEATURE_STRUCT, ///< IMU gyro scale matrix, see TY_GYRO_SCALE
        TY_STRUCT_IMU_CAM_TO_IMU        = 0x0607 | TY_FEATURE_STRUCT, ///< IMU camera to imu matrix, see TY_CAMERA_TO_IMU
        TY_ENUM_IMU_FPS                 = 0x0608 | TY_FEATURE_ENUM, ///< IMU fps, see TY_IMU_FPS_LIST
    
        TY_ENUM_DEPTH_QUALITY           = 0x0900 | TY_FEATURE_ENUM,  ///< the quality of generated depth, see TY_DEPTH_QUALITY
        TY_INT_FILTER_THRESHOLD         = 0x0901 | TY_FEATURE_INT,   ///< the threshold of the noise filter, 0 for disabled
        TY_INT_TOF_CHANNEL              = 0x0902 | TY_FEATURE_INT,   ///< the frequency channel of tof
    }TY_FEATURE_ID_LIST;


属性的数据结构示例：

1. depth、leftir / rightir、rgb 组件支持的分辨率如下：
   ::

      typedef enum TY_RESOLUTION_MODE_LIST
      {
          TY_RESOLUTION_MODE_160x100      = (160<<12)+100,    ///< 0x000a0078 
          TY_RESOLUTION_MODE_160x120      = (160<<12)+120,    ///< 0x000a0078 
          TY_RESOLUTION_MODE_240x320      = (240<<12)+320,    ///< 0x000f0140 
          TY_RESOLUTION_MODE_320x180      = (320<<12)+180,    ///< 0x001400b4
          TY_RESOLUTION_MODE_320x200      = (320<<12)+200,    ///< 0x001400c8
          TY_RESOLUTION_MODE_320x240      = (320<<12)+240,    ///< 0x001400f0
          TY_RESOLUTION_MODE_480x640      = (480<<12)+640,    ///< 0x001e0280
          TY_RESOLUTION_MODE_640x360      = (640<<12)+360,    ///< 0x00280168
          TY_RESOLUTION_MODE_640x400      = (640<<12)+400,    ///< 0x00280190
          TY_RESOLUTION_MODE_640x480      = (640<<12)+480,    ///< 0x002801e0
          TY_RESOLUTION_MODE_960x1280     = (960<<12)+1280,    ///< 0x003c0500
          TY_RESOLUTION_MODE_1280x720     = (1280<<12)+720,   ///< 0x005002d0
          TY_RESOLUTION_MODE_1280x800     = (1280<<12)+800,   ///< 0x00500320
          TY_RESOLUTION_MODE_1280x960     = (1280<<12)+960,   ///< 0x005003c0
          TY_RESOLUTION_MODE_1920x1080    = (1920<<12)+1080,   ///< 0x00780438
          TY_RESOLUTION_MODE_2592x1944    = (2592<<12)+1944,  ///< 0x00a20798
      }TY_RESOLUTION_MODE_LIST;


2. depth、leftir / rightir、rgb 组件支持的图像格式如下：
   ::

       typedef enum TY_PIXEL_FORMAT_LIST{
           TY_PIXEL_FORMAT_UNDEFINED   = 0,
           TY_PIXEL_FORMAT_MONO        = (TY_PIXEL_8BIT  | (0x0 << 24)), ///< 0x10000000
           TY_PIXEL_FORMAT_BAYER8GB    = (TY_PIXEL_8BIT  | (0x1 << 24)), ///< 0x11000000
           TY_PIXEL_FORMAT_BAYER8BG    = (TY_PIXEL_8BIT  | (0x2 << 24)), ///< 0x12000000
           TY_PIXEL_FORMAT_BAYER8GR    = (TY_PIXEL_8BIT  | (0x3 << 24)), ///< 0x13000000
           TY_PIXEL_FORMAT_BAYER8RG    = (TY_PIXEL_8BIT  | (0x4 << 24)), ///< 0x14000000
           TY_PIXEL_FORMAT_DEPTH16     = (TY_PIXEL_16BIT | (0x0 << 24)), ///< 0x20000000
           TY_PIXEL_FORMAT_YVYU        = (TY_PIXEL_16BIT | (0x1 << 24)), ///< 0x21000000, yvyu422
           TY_PIXEL_FORMAT_YUYV        = (TY_PIXEL_16BIT | (0x2 << 24)), ///< 0x22000000, yuyv422
           TY_PIXEL_FORMAT_MONO16      = (TY_PIXEL_16BIT | (0x3 << 24)), ///< 0x23000000, 
           TY_PIXEL_FORMAT_RGB         = (TY_PIXEL_24BIT | (0x0 << 24)), ///< 0x30000000
           TY_PIXEL_FORMAT_BGR         = (TY_PIXEL_24BIT | (0x1 << 24)), ///< 0x31000000
           TY_PIXEL_FORMAT_JPEG        = (TY_PIXEL_24BIT | (0x2 << 24)), ///< 0x32000000
           TY_PIXEL_FORMAT_MJPG        = (TY_PIXEL_24BIT | (0x3 << 24)), ///< 0x33000000
       }TY_PIXEL_FORMAT_LIST;


   .. note::
  
       #. 使用 TYSetEnum() 接口函数，通过设置图像传感器的 TY_ENUM_IMAGE_MODE 为 TY_IMAGE_MODE_格式_分辨率，即可同时设置图像的输出格式和分辨率。
       #. SDK 示例程序 DumpAllFeatures 可列出当前相机组件所支持的全部图像格式和分辨率。

3. 常用光学参数数据结构如下，可使用 TYGetStruct() 接口函数读取：
   ::

     typedef struct TY_VECT_3F
     {
         float   x;
         float   y;
         float   z;
     }TY_VECT_3F;
     
     ///  a 3x3 matrix  
     /// |.|.|.|
     /// | --|---|---|
     /// | fx|  0| cx|
     /// |  0| fy| cy|
     /// |  0|  0|  1|
     typedef struct TY_CAMERA_INTRINSIC
     {
         float data[3*3];
     }TY_CAMERA_INTRINSIC;
     
     /// a 4x4 matrix
     ///  |.|.|.|.|
     ///  |---|----|----|---|
     ///  |r11| r12| r13| t1|
     ///  |r21| r22| r23| t2|
     ///  |r31| r32| r33| t3|
     ///  | 0 |   0|   0|  1|
     typedef struct TY_CAMERA_EXTRINSIC
     {
         float data[4*4];
     }TY_CAMERA_EXTRINSIC;
     
     ///camera distortion parameters
     typedef struct TY_CAMERA_DISTORTION
     {
         float data[12];///<Definition is compatible with opencv3.0+ :k1,k2,p1,p2,k3,k4,k5,k6,s1,s2,s3,s4
     }TY_CAMERA_DISTORTION;
     
     
     ///camera 's cailbration data
     ///@see TYGetStruct
     typedef struct TY_CAMERA_CALIB_INFO
     {
       int32_t intrinsicWidth;
       int32_t intrinsicHeight;
       TY_CAMERA_INTRINSIC   intrinsic;  // TY_STRUCT_CAM_INTRINSIC
       TY_CAMERA_EXTRINSIC   extrinsic;  // TY_STRUCT_EXTRINSIC_TO_LEFT_IR
       TY_CAMERA_DISTORTION  distortion; // TY_STRUCT_CAM_DISTORTION
     }TY_CAMERA_CALIB_INFO;
