
图像获取流程
=============

深度相机的配置和图像获取流程如下图所示：
    
.. figure:: ../image/camportapiflowchart.png
    :width: 700px
    :align: center
    :alt: 图像获取流程图
    :figclass: align-center

    图像获取流程图


下面以 SDK 示例程序 Simpleview_FetchFrame 为例详细说明图像获取流程。

初始化API
++++++++++
TYInitLib 初始化设备对象等数据结构。

打开设备
+++++++++

1. 获取设备列表

|  初次获取设备信息时可以通过 selectDevice() 查询已连接的设备数量，并获得所有已连接的设备列表。
| 

2. 打开接口

|  TYOpenInterface 打开接口。
| 

3. 打开设备

|  TYOpenDevice 打开设备。
|  TYOpenDeviceWithIP 打开网络设备。
| 



配置组件
++++++++++++++++++++

1. 查询设备的组件状态

| TYGetComponentIDs 获取设备支持的组件信息。
| TYGetEnabledComponents 获取已经打开的组件信息。
|

2. 配置组件

| TYEnableComponents 使能组件。
| TYDisableComponents 关闭组件。
|
  设备打开后，默认只有虚拟组件 TY_COMPONENT_DEVICE 是使能状态。多个组件可以通过 **位或** 方式同时使能。
  
  ::
  
      int32_t componentIDs = TY_COMPONENT_DEPTH_CAM | TY_COMPONENT_RGB_CAM;
      TYEnableComponents(hDevice, componentIDs);

1. 配置属性
   
   * 查询指定属性的信息：

     TYGetFeatureInfo() 通过填充结构体 TY_FEATURE_INFO 来获取指定组件的指定属性的信息。如果指定组件不包含所指定的属性，则 TY_FEATURE_INFO 中 isValid 值为 false；如果该组件包含所指定的参数，则 TY_FEATURE_INFO 中 isValid 值为 true。也可以通过 TYGetIntRange 等具体参数类型的 API 接口查询指定功能参数的信息。

   * 常用读写属性函数如下：
     ::
         
         TYGetIntRange
         TYGetInt
         TYSetInt
         TYGetFloatRange
         TYGetFloat
         TYSetFloat
         TYGetEnumEntryCount
         TYGetEnumEntryInfo
         TYGetEnum
         TYSetEnum
         TYGetBool
         TYSetBool
         TYGetStringLength
         TYGetString
         TYSetString
         TYGetStruct
         TYSetStruct
         
     **示例**
     
     调用 TYSetEnum() 设置深度图像传感器输出数据的格式和分辨率：
     ::
        
        LOGD("=== Configure feature, set resolution to 640x480.");
        ASSERT_OK(TYSetEnum(hDevice, TY_COMPONENT_DEPTH_CAM, TY_ENUM_IMAGE_MODE, TY_IMAGE_MODE_DEPTH16_640x480));


帧缓冲管理
+++++++++++++++


1. 调用 API 查询当前配置下每个帧缓冲的大小。
   ::
       
       uint32_t frameSize;
       ASSERT_OK( TYGetFrameBufferSize(hDevice, &frameSize) );
       LOGD("     - Get size of framebuffer, %d", frameSize);
    
2. 分配深度数据存储空间。
    
   按照实际查询函数返回的帧缓冲的大小分配两组 frameBuffer 空间，并传递给底层驱动缓冲队列，作为数据获取的通道。
       
   驱动内部维护一个缓冲队列（frameBuffer Queue），每帧数据传出时会将填充好的 frameBuffer 作 Dequeue 操作，并完全传出给用户使用。用户完成该帧图像数据处理后，需做 Enqueue 动作以返还该 frameBuffer 给驱动层缓冲队列。用户需要保证新的一帧数据到来时驱动的缓冲队列不为空，否则该帧数据将被丢弃。
   ::
   
       LOGD("     - Allocate & enqueue buffers");
       char* frameBuffer[2];
       frameBuffer[0] = new char[frameSize];
       frameBuffer[1] = new char[frameSize];
       LOGD("     - Enqueue buffer (%p, %d)", frameBuffer[0], frameSize);
       ASSERT_OK( TYEnqueueBuffer(hDevice, frameBuffer[0], frameSize) );
       LOGD("     - Enqueue buffer (%p, %d)", frameBuffer[1], frameSize);
       ASSERT_OK( TYEnqueueBuffer(hDevice, frameBuffer[1], frameSize) );

    
回调函数注册
++++++++++++

TYRegisterEventCallback

使用回调函数的方式获取图像数据时，需要注册该函数，当图像数据到达后，该回调函数会主动被执行。在使用主动获取图像模式时，需要调用该函数注册回调函数为 NULL。
::

   LOGD("Register event callback");
   ASSERT_OK(TYRegisterEventCallback(hDevice, eventCallback, NULL))


配置工作模式
++++++++++++

根据实际需要配置深度相机工作模式，详情请参考 :ref:`工作模式配置 <trigger-config-label>`。
    

启动深度采集
++++++++++++

TYStartCapture

如果深度相机工作在模式 1 下，可以使用软件触发接口函数 TYSendSoftTrigger()，通过 USB 接口或者以太网接口发送指令，控制相机图像采集的时机。

获取帧数据
++++++++++
    
TYFetchFrame

主动获取深度数据模式下，应用可调用该接口获取深度数据。注意回调函数模式下不需要调用。获取数据后，用户程序进行运算处理时，应采用独立线程，避免堵塞图像获取线程的运转。


停止采集
+++++++++

TYStopCapture 停止图像数据采集，相机停止深度数据计算和输出。

关闭设备
++++++++

TYCloseDevice 关闭设备，TYCloseInterface 释放占用的接口。

释放API
++++++++

TYDeinitLib 释放 API 后，需要释放分配的内存资源，避免内存泄露。




