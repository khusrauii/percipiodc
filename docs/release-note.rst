Release Note
===================

V 2.3.1
---------

1. Add the *Percipio Viewer User Guide*.
2. Add the Troubleshooting section.
3. Add the specifications of new 3D cameras and modified the specifications of PS801.
4. Modified the index.