Percipio 技术文档
====================

图漾科技（Percipio）是全球领先的 3D 机器视觉供应商，公司致力于通过技术创新，为客户提供超性价比的3D视觉组件。如今，Percipio 已经为全球数千家客户提供稳定可靠的产品和服务，行业遍及工业、物流、医疗、农业、矿业、零售等各个领域。

本文档主要提供软硬环境搭建说明、API 接口说明和深度相机硬件参考等信息。

.. card:: 

   .. toctree::
      :maxdepth: 1
      :caption: 快速入门
   
      getstarted/sdk-compile
      getstarted/hardware-connection
      getstarted/application
      getstarted/glossary
      apiguides/index


.. card:: 

   .. toctree::
      :maxdepth: 2
      :caption: API 指南
   
      
      

.. card:: 

   .. toctree::
      :maxdepth: 2
      :caption: 硬件参考 
   
      硬件原理 <hwreference/hwprinciple>
      产品规格 <hwreference/hwspecs>

.. card:: 

   .. toctree::
      :maxdepth: 2
      :caption: 看图软件
   
      Percipio Viewer 用户指南 <viewer>

.. card:: 

   .. toctree::
      :maxdepth: 1
      :caption: 技术支持  
   
      故障排除 <troubleshooting/index>
      联系我们 <about>
      声明 <COPYRIGHTS>






   



