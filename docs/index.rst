【迁移通知】
====================
| 为了确保本文档访问的长期稳定性和便捷性，本文档已迁移到官网域名链接下，此网站的网页不再维护更新，请您访问 `图漾官方在线文档 <http://doc.percipio.xyz/cam/latest/>`_，由此给大家带来的不便敬请谅解，谢谢。
| In order to ensure the long-term stability and convenience of document access, this document has been migrated to the official website. This page is no longer maintained, please visit `Percipio official online document <http://doc.percipio.xyz/cam/latest/>`_  for the latest product information and technical notes，thank you.
==================================================================================================================================================================================================


Percipio 技术文档
====================

图漾科技（Percipio）是全球领先的 3D 机器视觉供应商，公司致力于通过技术创新，为客户提供超性价比的3D视觉组件。如今，Percipio 已经为全球数千家客户提供稳定可靠的产品和服务，行业遍及工业、物流、医疗、农业、矿业、零售等各个领域。



.. card::

   .. toctree::
      :maxdepth: 1
      :caption: 技术支持  


      故障排除 <troubleshooting/index>
      联系我们 <about>
      声明 <COPYRIGHTS>

