PERCIPIO Depth Camera document
===============================

This is Sphinx documents for Percipio depth camera SDK written with reStructuredText and distributed on Read the Docs. This document intend to help users set up the software environment for development of applications using Percipio Depth Camera.


